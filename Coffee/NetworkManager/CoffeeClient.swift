//
//  CoffeeClient.swift
//  Coffee
//
//  Created by mohamed sayed on 12/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation
import Alamofire

class CoffeeClient{
    
    static func getTokenAccess(completion: @escaping (_ result:AccessTokenResponse?,_ error:String?)->Void){
        let url = "https://c.quick.sa.com/API/Login/GetAccessToken"
        let parameter :[String:String] = [
            "UserName":"cafestation",
            "Password":"cafestation2020"
        ]
        
        let header:[String:String] = [
            "Content-Type":"application/x-www-form-urlencoded"]
        
        Alamofire.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            
            switch response.result{
            case .success(let value):
                let decoder = JSONDecoder()
                
                do{
                    let jsonData = try? JSONSerialization.data(withJSONObject:value)
                    
                    let valueObject  = try decoder.decode(AccessTokenResponse.self, from: jsonData!)
                    completion(valueObject,nil)
                }catch let error{
                    completion(nil,error.localizedDescription)
                }
                
            case .failure(let error):
                completion(nil,error.localizedDescription)
            }
        }
    }
    
    
    static func getCitiesAPI(completion: @escaping (_ result:CitiesResponse?,_ error:String?)->Void){
        
        Alamofire.request(Router.getCities).responseJSON(completionHandler: { (reponse) in
            
            switch reponse.result{
            case .success(let value):
                let decoder = JSONDecoder()
                do{
                    let jsonData = try? JSONSerialization.data(withJSONObject:value)
                    
                    let valueObject  = try decoder.decode(CitiesResponse.self, from: jsonData!)
                    completion(valueObject,nil)
                    print("Coffee Cities valueObject \(valueObject)")
                }catch let error{
                    completion(nil,error.localizedDescription)
                    print("Coffee error Data  \(error)")
                }
            case .failure(let error):
                completion(nil,error.localizedDescription)
            }
            
        })
    }
    
    static func getShippingCost(cityID:Int,completion: @escaping (_ result:ShippingCostResponse?,_ error:String?)->Void){
        
        let token = UserDefaults.standard.string(forKey: Constants.CoffeeConstant.accessToken.rawValue)
        
        let url = "https://c.quick.sa.com/API/V3/Store/Shipment/GetShippingCost"
        let parameter :[String:String] = [
            "CityId":"\(cityID)",
            "PaymentMethodId":"4"
        ]
        
        let header:[String:String] = [
            "Content-Type":"application/x-www-form-urlencoded",
            "Authorization":"Bearer \(token ?? "")"
        ]
        
        Alamofire.request(url, method: .post, parameters: parameter, headers: header).responseJSON { (response) in
            
            switch response.result{
            case .success(let value):
                let decoder = JSONDecoder()
                
                do{
                    let jsonData = try? JSONSerialization.data(withJSONObject:value)
                    
                    let valueObject  = try decoder.decode(ShippingCostResponse.self, from: jsonData!)
                    completion(valueObject,nil)
                }catch let error{
                    completion(nil,error.localizedDescription)
                }
                
            case .failure(let error):
                completion(nil,error.localizedDescription)
            }
        }
    }
    
    
    static func creatShipment(customerName:String,phoneNumber:String,deliveryPrice:String, customerAddress:String,lat:String,long:String,cityID:String,paymentMethodId:String){

        let date = Date()
        let dateFormatte = DateFormatter()
        dateFormatte.dateFormat = "yyyy/MM/dd HH:mm"
        let currentDate = dateFormatte.string(from: date)
        
        let url = "https://c.quick.sa.com/API/V3/Store/Shipment"
        
        let token = UserDefaults.standard.string(forKey: Constants.CoffeeConstant.accessToken.rawValue)
        
        
        let parameters = "{\r\n      SandboxMode: true,\r\n        CustomerName: \"\(customerName)\",\r\n        CustomerPhoneNumber: \"\(phoneNumber)\",\r\n        PreferredReceiptTime: \"\(currentDate)\",\r\n        PreferredDeliveryTime: \"\(currentDate)\",\r\n        PaymentMethodId: \(paymentMethodId),  \r\n        ShipmentContentValueSAR:\(deliveryPrice),\r\n        ShipmentContentTypeId: 4, \r\n        CustomerLocation: {\r\n            Desciption: \"\(customerAddress)\",\r\n            Longitude: \"\(lat)\",\r\n            Latitude: \"\(long)\",\r\n            CountryId: 1,\r\n            CityId:\(cityID) \r\n        }\r\n}"
        
        let postData = parameters.data(using: .utf8)
        
        var request = URLRequest(url: URL(string: url)!,timeoutInterval: Double.infinity)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("Bearer \(token ?? "")", forHTTPHeaderField: Constants.HTTPHeaderField.authorization.rawValue)
        
        request.httpMethod = "POST"
        request.httpBody = postData
        
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            guard let data = data else {
                print(String(describing: error))
                return
            }

            print("CoffeeClient \(String(data: data, encoding: .utf8)!)")
        }
        task.resume()
    }
}

