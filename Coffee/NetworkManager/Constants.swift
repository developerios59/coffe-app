//
//  Constants.swift
//  Coffee
//
//  Created by mohamed sayed on 12/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Alamofire

struct Constants {
    
    struct ProductionServer {
        static let baseURL = "https://c.quick.sa.com/API/"
    }
    
    enum HTTPHeaderField: String {
        case contentType      = "Content-Type"
        case acceptType       = "Accept"
        case acceptEncoding   = "Accept-Encoding"
        case authorization    = "Authorization"
    }
    
    enum ContentType: String {
        case json = "application/x-www-form-urlencoded"
    }
    
    enum CoffeeConstant :String {
        case userName = "cafestation"
        case password = "cafestation2020"
        case accessToken = "Token"
        case taxPrice = "taxPrice"
        case totlaPrice = "totlaPrice"
    }
    
    enum APIParameterKey :String {
        case userName = "UserName"
        case  password = "Password"
    }
}

struct URLWithFile<T: Codable> {
    var url: String
    var httpMethod: HTTPMethod
    var parameters:Parameters
    var header:HTTPHeaders
    var images: [String:Data]
}

enum RequestParams {
    case body(_:Parameters)
    case url(_:Parameters)
    case bodyAndURL(body:Parameters , url:Parameters)
}
