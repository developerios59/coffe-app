//
//  Router.swift
//  Coffee
//
//  Created by mohamed sayed on 12/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.

import Foundation
import Alamofire
//home
let DeviceID = UIDevice.current.identifierForVendor?.uuidString

enum Router: URLRequestConvertible {
    
    case getCities
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        switch self {
        case .getCities:
        return .get
         default:
            return .post
        }
    }

    // MARK: - Path
    private var path: String {
        switch self {
        case .getCities:
            return "V3/GetConsistentData" 
        }
    }
    // MARK: - Parameters
    private var parameters: RequestParams? {
        let constants = Constants.APIParameterKey.self
        switch self {
//        case.getCities:
        default:
            return nil
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        
        let url = try Constants.ProductionServer.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        let token = UserDefaults.standard.string(forKey: Constants.CoffeeConstant.accessToken.rawValue)
        
        urlRequest.setValue("Bearer \(token ?? "")", forHTTPHeaderField: Constants.HTTPHeaderField.authorization.rawValue)

        urlRequest.setValue(Constants.ContentType.json.rawValue, forHTTPHeaderField: Constants.HTTPHeaderField.contentType.rawValue)
                
        if let parameters = parameters {
            switch parameters {
            case .body(let parameters):
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
                } catch {
                    throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                }
                
            case .url(let parameters):
                let queryParams = parameters.map { pair  in
                    return URLQueryItem(name: pair.key, value: "\(pair.value)")
                }
                var components = URLComponents(string:url.appendingPathComponent(path).absoluteString)
                components?.queryItems = queryParams
                urlRequest.url = components?.url
                
            case .bodyAndURL(let bodyParameters, let urlParameters):
                do {
                    urlRequest.httpBody = try JSONSerialization.data(withJSONObject: bodyParameters, options: [])
                } catch {
                    throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
                }
                
                let queryParams = urlParameters.map { pair  in
                    return URLQueryItem(name: pair.key, value: "\(pair.value)")
                }
                var components = URLComponents(string:url.appendingPathComponent(path).absoluteString)
                components?.queryItems = queryParams
                urlRequest.url = components?.url
            }
        }
        return urlRequest
    }
}



