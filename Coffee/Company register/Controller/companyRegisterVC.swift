//
//  companyRegisterVC.swift
//  Coffee
//
//  Created by Mustafa on 10/18/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH


protocol getCategoryProtcol {
    func getCategory(categoryId:String,name:String)
}

class companyRegisterVC: UIViewController , NVActivityIndicatorViewable {
    
    @IBOutlet weak var cafeNameTF: UITextFieldX!
    @IBOutlet weak var mobileNumberTF: UITextFieldX!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var productTypesTF: UITextFieldX!
    @IBOutlet weak var notes: UITextView!
    @IBOutlet weak var cafeImg: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    
    
    
    
    var cafeImageData = Data()
    
    var cafeImgValid = ""
    
    var note = ""
    var categoryId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productTypesTF.delegate = self
        notes.delegate = self
        
        setupUI()
        let imagetap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
            cafeImg.addGestureRecognizer(imagetap)
              
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    func setupUI() {
        self.tabBarController?.tabBar.isHidden = true
               self.tabBarController?.tabBar.isTranslucent = true
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
        cafeNameTF.text = ""
        mobileNumberTF.text = ""
        emailTF.text = ""
        productTypesTF.text = ""
        notes.textColor = #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1)
        notes.text = "notes".localized
        note = ""
        cafeImgValid = ""
        categoryId = ""
        cafeImg.image  = UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "add_Img_en" : "add_img_ar")
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            
            self.cafeImg.image = image
            self.cafeImageData = image.jpegData(compressionQuality: 0.5)!
            self.cafeImgValid = "Valid"
        }
    }
    
   
    
   
    @IBAction func didSelectConfirmBu(_ sender: Any) {
        let response = Validation.shared.validate(values:(ValidationType.emptyName , cafeNameTF.text!),(ValidationType.phoneNo, mobileNumberTF.text!),(ValidationType.email , emailTF.text!),(ValidationType.emptyCategory , categoryId),(ValidationType.emptyNotes , note),(ValidationType.emptyCafeImage , cafeImgValid))
        
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        
        
         let uploadData = [UploadData(data: cafeImageData, fileName: "image", mimeType: "image/png", name: "image")]

        
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.companyRegister(lang: "en".localized, name: cafeNameTF.text!, phone: mobileNumberTF.text!, email: emailTF.text!, category_id: categoryId, notes: notes.text).send(companyRegisterModel.self, data: uploadData, progress: nil) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    print(error)
                case .success(let model):
                    
                    self.setupUI()
                    Helper.standard.displayAlert(title: "", message: model.data, vc: self)
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
}

extension companyRegisterVC : UITextFieldDelegate {
    
     func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == productTypesTF {
            self.view.endEditing(true)
            let popupScene = popUpVC.instantiate(fromAppStoryboard: .CompanyRegister)
                popupScene.delegate = self
            self.present(popupScene, animated: true, completion: nil)
            return false
        }
        return true
    }

}

extension companyRegisterVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1) {
            textView.text = nil
            note = "Valid"
            textView.textColor = UIColor.white
        }
    }
       
       
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1)
            note = ""
            textView.text = "notes".localized
        }
    }

}


extension companyRegisterVC : getCategoryProtcol {
    func getCategory(categoryId:String,name:String) {
        self.categoryId = categoryId
        self.productTypesTF.text = name
    }
    
    
}
