//
//  popUpVC.swift
//  Mazad
//
//  Created by saleh on 2/18/20.
//  Copyright © 2020 saleh. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class popUpVC: UIViewController ,NVActivityIndicatorViewable {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLB: UILabel!
 
    var categories = [categoryData]()
    
    var delegate : getCategoryProtcol?

    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        tableView.dataSource = self
        
        getCategories()
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    func getCategories() {
           if Reachability.isConnectedToNetwork(){
               self.startAnimating()
               UserRouter.categories(lang: "en".localized).send(categoryModel.self) { (response) in
                   self.stopAnimating()
                   switch response {
                   case .failure(let error):
                       // TODO: - Handle error as you want, printing isn't handling.
                       print(error)
                   case .success(let model):
                       print(model)
                       if model.value == "0" {
                           
                       }else if model.value == "1" {
                           self.categories = model.data
                        self.tableView.reloadData()
                       }
                       
                   }
               }
               
           }else{
               Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
           }
       }
    
    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}


extension popUpVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "popUpCell", for: indexPath)  as! popUpCell
        cell.configureCell(data: self.categories[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.delegate?.getCategory(categoryId: String(self.categories[indexPath.row].id),name: self.categories[indexPath.row].name)
        self.dismiss(animated: true, completion: nil)
        
    }
    
}
