//
//  popUpCell.swift
//  Coffee
//
//  Created by Mustafa on 10/18/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class popUpCell: UITableViewCell {

    @IBOutlet weak var nameLB: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(data:categoryData) {
        nameLB.text = data.name
    }

}
