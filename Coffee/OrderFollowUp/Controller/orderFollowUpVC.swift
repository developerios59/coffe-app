//
//  orderFollowUpVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH


protocol handleProtocol {
    func pop()
}

class orderFollowUpVC: UIViewController ,NVActivityIndicatorViewable{
    
    @IBOutlet weak var approvedPendingImg: UIImageView!
    @IBOutlet weak var acceptStatusIMg: UIImageView!
    @IBOutlet weak var delegateStatusImg: UIImageView!
    @IBOutlet weak var completedStatusImg: UIImageView!
    @IBOutlet weak var confirmBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    
    var orderId = ""
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
               self.tabBarController?.tabBar.isTranslucent = true
        
          backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        getTrackStatus(status: "", confirm: "")
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
    
    func getTrackStatus(status:String,confirm:String) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.orderTrack(lang: "en".localized, order_id: orderId,status:status,confirm:confirm).send(trackOrderModel.self)  { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if confirm == "1" {
                            let serviceRatingScene = serviceRatingVC.instantiate(fromAppStoryboard: .OrderFollowUp)
                            serviceRatingScene.orderId = self.orderId
                            serviceRatingScene.delegate = self
                            self.present(serviceRatingScene, animated: true, completion: nil)
                        }else{
                        switch model.data {
                        case "current" :
                             self.confirmBtn.isHidden = true
                             self.approvedPendingImg.image = UIImage(named: "selected")
                        case "accept":
                            self.confirmBtn.isHidden = true
                            self.acceptStatusIMg.image = UIImage(named: "selected")
                        case "delivery_order":
                             self.confirmBtn.isHidden = false
                            self.acceptStatusIMg.image = UIImage(named: "selected")
                            self.delegateStatusImg.image = UIImage(named: "selected")
                        case "completed":
                             self.confirmBtn.isHidden = true
                            self.acceptStatusIMg.image = UIImage(named: "selected")
                            self.delegateStatusImg.image = UIImage(named: "selected")
                            self.completedStatusImg.image = UIImage(named: "selected")
                        default:
                            break
                        }
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
    @IBAction func didSelectConfirmBu(_ sender: Any) {
        self.getTrackStatus(status: "completed", confirm: "1")
        
    }
    
}


extension orderFollowUpVC : handleProtocol {
    func pop() {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
}
