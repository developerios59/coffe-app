//
//  serviceRatingVC.swift
//  Coffee
//
//  Created by Mustafa on 10/17/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import Cosmos
import NVActivityIndicatorView

class serviceRatingVC: UIViewController ,NVActivityIndicatorViewable{

    
    @IBOutlet weak var rateView: CosmosView!
  
    
    var orderId = ""
    
    var delegate : handleProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    

    @IBAction func didSelectConfirmBu(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.orderRate(lang: "en".localized, order_id: orderId, rate: String(rateView.rating)).send(orderRateModel.self)  { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self)
                    }else if model.value == "1" {
                        self.dismiss(animated: true, completion: nil)
                        self.delegate?.pop()
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    

}
