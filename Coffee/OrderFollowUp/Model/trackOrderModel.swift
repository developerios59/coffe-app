//
//  trackOrderModel.swift
//  Coffee
//
//  Created by Mustafa on 10/17/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


struct trackOrderModel: Codable,CodableInit {
    let value, key : String
    let msg:String?
    let data: String?
}
