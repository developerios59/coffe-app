//
//  orderRateModel.swift
//  Coffee
//
//  Created by Mustafa on 10/21/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - orderRateModel
struct orderRateModel : Codable ,CodableInit{
    let value, key:String
    let data,msg: String?
    
}
