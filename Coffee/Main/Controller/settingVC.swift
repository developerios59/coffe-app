//
//  settingVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class settingVC: UIViewController ,NVActivityIndicatorViewable {
    
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var emailLB: UILabel!
    @IBOutlet weak var tableView: IntrinsicTableView!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var signBtn: UIButton!
    @IBOutlet weak var profileView: UIView!
    
    let settings = Bundle.main.decode([settingModel].self, from: "settings.json")
    let userId = Helper.standard.userId()
    let deviceId = Helper.standard.deviceID()
    
    var whatsAppNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
       

        getWhatsappNumber()
        
       
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        self.navigationController?.navigationBar.isHidden = true
        
        if self.userId == "" {
            self.signBtn.setTitle("sign in".localized, for: .normal)
            self.signBtn.setImage(UIImage(named: "enter"), for: .normal)
            Helper.standard.showVisitorPopUp(vc: self)
            profileView.isHidden = true
        }else{
            self.signBtn.setImage(UIImage(named: "log"), for: .normal)
            getProfileData()
        }
    }
    
    func getProfileData() {
        
        img.layer.borderWidth = 1
        img.layer.borderColor = Helper.standard.hexStringToUIColor(hex: "F8F8F8").cgColor
        img.layer.cornerRadius = img.frame.size.height / 2
        let userName = UserDefaults.standard.value(forKey: "userName") as? String
        let email = UserDefaults.standard.value(forKey: "email") as? String
        if   let avatar = UserDefaults.standard.value(forKey: "avatar") as? String {
            self.img.kf.setImage(with: URL(string: avatar ))
        }
        nameLB.text = userName
        emailLB.text = email
    }
    
    
    func getWhatsappNumber() {
        
         if Reachability.isConnectedToNetwork(){
             self.startAnimating()
            UserRouter.whatsApp.send(whatsAppModel.self) { (response) in
                 self.stopAnimating()
                 switch response {
                 case .failure(let error):
                     // TODO: - Handle error as you want, printing isn't handling.
                     print(error)
                 case .success(let model):
                     print(model)
                     if model.value == "0" {
                         Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self)
                     }else  if model.value == "1" {
                        self.whatsAppNumber = model.data!
                    
                     }
                 }
             }
         }else{
             Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
         }
    }
    
    
    @IBAction func didSelectOrdersBu(_ sender: Any) {
        if self.userId == "" {
            Helper.standard.showVisitorPopUp(vc: self)
        }else{
        let orderScene = ordersVC.instantiate(fromAppStoryboard: .Orders)
            
        self.navigationController?.pushViewController(orderScene, animated: true)
        }
    }
    

    
    @IBAction func didSelectFavoriteBu(_ sender: Any) {
        if self.userId == "" {
            Helper.standard.showVisitorPopUp(vc: self)
              }else{
        let favouriteScene =   favoriteVC.instantiate(fromAppStoryboard: .Favourite)
        self.navigationController?.pushViewController(favouriteScene, animated: true)
        }
    }
    
    
    @IBAction func didSelectSignOutBu(_ sender: Any) {
        if userId == "" {
            let signInScene = signInVC.instantiate(fromAppStoryboard: .authentication)
            self.navigationController?.pushViewController(signInScene, animated: true)
        }else{
        let alert = UIAlertController(title: "Are you Sure?".localized, message: "", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { (_) in
            // Api Request
            var signOutResponse: HandleResponse<logOutModel> {
                return {[weak self] (response) in
                    self?.stopAnimating()
                    switch response {
                    case .failure(let error):
                        // TODO: - Handle error as you want, printing isn't handling.
                        print(error)
                    case .success(let model):
                        //  print(model)
                        if model.value == "0" {
                            Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                        }else if model.value == "1" {
                            UserDefaults.standard.removeObject(forKey: "ToKen")
                             UserDefaults.standard.removeObject(forKey: "UserId")
                            UserDefaults.standard.removeObject(forKey: "avatar")
                            UserDefaults.standard.removeObject(forKey: "email")
                            UserDefaults.standard.removeObject(forKey: "userName")
                            UserDefaults.standard.removeObject(forKey: "deviceID")
                            
                            let signInScene =  signInVC.instantiate(fromAppStoryboard: .authentication)
                            
                            let nav = UINavigationController(rootViewController: signInScene)
                            
                            UIApplication.shared.windows.first?.rootViewController = nav
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                    }
                }
            }
            
            if Reachability.isConnectedToNetwork(){
                self.startAnimating()
                UserRouter.logOut( device_id: self.deviceId, device_type: "ios", lang: "en".localized).send(logOutModel.self, then: signOutResponse)
            }else{
                Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
            }
        }))
        
        alert.addAction(UIAlertAction(title: "No".localized, style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    
    func share() {
           let textToShare = "Coffee App is awesome!  Check out this App!"
           
           if let appURL = NSURL(string: "itms-apps://itunes.apple.com/app/id1497454302") {
               let objectsToShare: [Any] = [textToShare, appURL]
               let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
               self.present(activityVC, animated: true, completion: nil)
           }
       }
    
}

extension settingVC : UITableViewDataSource , UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width , height: 60))
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: view.frame.width - 30, height: 50))
        label.font = UIFont(name: "JF Flat", size: 12)
        label.text =  MOLHLanguage.currentAppleLanguage().contains("en") ? settings[section].enName : settings[section].arName
        label.textColor = .white
        view.addSubview(label)
        return view
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return settings.count
        
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings[section].items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "settingCell", for: indexPath) as! settingCell
        
        cell.configureCell(data: self.settings[indexPath.section].items[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
            
        case 0 :
        //    Company Regesteration
            let companyRegisterScene =  companyRegisterVC.instantiate(fromAppStoryboard: .CompanyRegister)
            self.navigationController?.pushViewController(companyRegisterScene, animated: true)
        case 1:
            // Profile
            if self.userId == "" {
            Helper.standard.showVisitorPopUp(vc: self)
              }else{
            let profileScene =  profileVC.instantiate(fromAppStoryboard: .Profile)
            self.navigationController?.pushViewController(profileScene, animated: true)
            }
        case 2:
            // App INFO AND Notification
            switch indexPath.row {
            case 2:
                if self.userId == "" {
                    Helper.standard.showVisitorPopUp(vc: self)
                }else{
                    let notificationScene =  notificationVC.instantiate(fromAppStoryboard: .Notification)
                    self.navigationController?.pushViewController(notificationScene, animated: true)
                }
            default:
                let appInfoScene =  appInfoVC.instantiate(fromAppStoryboard: .AppInfo)
                appInfoScene.typeIndex = indexPath.row
                self.navigationController?.pushViewController(appInfoScene, animated: true)
            }
        case 3:
            switch indexPath.row {
            case 0:
                // whats app
               
                    if   let urlWhats = "whatsapp://send?phone=\(self.whatsAppNumber)&text=Hello".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                        if UIApplication.shared.canOpenURL(URL(string: urlWhats)! ) {
                            UIApplication.shared.open(URL(string: urlWhats)!, options: [:], completionHandler: nil)
                        } else {
                            if   let urlWhats = "https://wa.me/\(self.whatsAppNumber)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                                if UIApplication.shared.canOpenURL(URL(string: urlWhats)! ) {
                                    UIApplication.shared.open(URL(string: urlWhats)!, options: [:], completionHandler: nil)
                                }
                            }
                        }
                    }
                
            case 1:
                //Contact US
                let contactUsScene =  contactUsVC.instantiate(fromAppStoryboard: .ContactUs)
                self.navigationController?.pushViewController(contactUsScene, animated: true)
            case 2:
                //Follow US
                let followScene =  followUsVC.instantiate(fromAppStoryboard: .ContactUs)
                self.navigationController?.pushViewController(followScene, animated: true)
                
            case 3:
                // App Share
               share()
            default:
                break
            }
        default:
            break
        }
        
        
    }
    
}
