//
//  mainVC.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import ImageSlideshow
import NVActivityIndicatorView
import AZTabBar

class mainVC: UIViewController ,NVActivityIndicatorViewable {
    
    @IBOutlet weak var imageSlider: ImageSlideshow!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    @IBOutlet weak var brandsCollectionView: ContentSizedCollectionView!
    
    var brands = [Brand]()
    var categories = [Brand]()
    
    var imageSliderArr = [KingfisherSource]()
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
        getHomeData()
    }
    
    func setupUI() {
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        brandsCollectionView.delegate = self
        brandsCollectionView.dataSource = self
        
        imageSlider.layer.cornerRadius = 8
        imageSlider.delegate = self
        imageSlider.slideshowInterval = 3.0
        imageSlider.pageIndicator = nil
        imageSlider.contentScaleMode = .scaleToFill
    }
    
    
    func getHomeData() {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.home(lang: "en".localized).send(mainModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        self.brandsCollectionView.isHidden = model.brands.isEmpty ? true : false
                        self.categoriesCollectionView.isHidden = model.categories.isEmpty ? true : false
                        self.brands = model.brands
                        self.categories  = model.categories
                        self.brandsCollectionView.reloadData()
                        self.categoriesCollectionView.reloadData()
                        
                        if self.imageSliderArr.isEmpty {
                            for image in model.sliders {
                                self.imageSliderArr.append(KingfisherSource(urlString: image)!)
                                self.imageSlider.setImageInputs(self.imageSliderArr)
                                self.imageSlider.reloadInputViews()
                                
                            }
                        }
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
}


extension mainVC : UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == brandsCollectionView {
            return brands.count
        }else {
            return categories.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == brandsCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "brandCell", for: indexPath) as! brandCell
            cell.configureCell(data: self.brands[indexPath.row])
            return cell
        }else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCell", for: indexPath) as! categoryCell
             cell.configureCell(data: self.categories[indexPath.row])
            return cell
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == brandsCollectionView {
            let width = collectionView.frame.width - 20
            return CGSize(width: width / 2, height: 120)
        }else{
            return CGSize(width: ((collectionView.frame.width ) / CGFloat(self.categories.count) ), height: collectionView.frame.height)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
         if collectionView == brandsCollectionView {
            let categoryScene =  categoryVC.instantiate(fromAppStoryboard: .Category)
                categoryScene.categoryName = self.brands[indexPath.row].name
                categoryScene.categoryId =  String(self.brands[indexPath.row].id)
                categoryScene.categoryImage =  self.brands[indexPath.row].image
            categoryScene.type = "brand"
            self.navigationController?.pushViewController(categoryScene, animated: true)
         }else{
            if categories[indexPath.row].id == 1 {
                 let baristaScene =  baristaVC.instantiate(fromAppStoryboard: .Barista)
                 self.navigationController?.pushViewController(baristaScene, animated: true)
                
            }else{
            let categoryScene =  categoryVC.instantiate(fromAppStoryboard: .Category)
                categoryScene.categoryName = self.categories[indexPath.row].name
                categoryScene.categoryId =  String(self.categories[indexPath.row].id)
                categoryScene.categoryImage =  self.categories[indexPath.row].image
                 categoryScene.type = "category"
                self.navigationController?.pushViewController(categoryScene, animated: true)
            }
        }
    }
    
}


extension mainVC : ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
// pageControl.page = page
        
        //set(progress: page, animated: true)
    }
}



