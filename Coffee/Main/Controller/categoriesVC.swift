//
//  categoriesVC.swift
//  Coffee
//
//  Created by Mustafa on 10/8/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class categoriesVC: UIViewController ,NVActivityIndicatorViewable {

    @IBOutlet weak var collectionView: UICollectionView!
    
    var categories = [categoryData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.dataSource = self
        collectionView.delegate = self
     getCategories() 
        
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
    }
    
    
    func getCategories() {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.categories(lang: "en".localized).send(categoryModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            
                        }else{
                            self.categories = model.data
                            self.collectionView.reloadData()
                        }
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    


}


extension categoriesVC : UICollectionViewDataSource , UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriesCell", for: indexPath) as! categoriesCell
        
        cell.configureCell(data: self.categories[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         
               let width = collectionView.frame.width - 20
               return CGSize(width: width / 2, height: width / 2)
    }
       
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if categories[indexPath.row].id == 1 {
            let baristaScene =  baristaVC.instantiate(fromAppStoryboard: .Barista)
            self.navigationController?.pushViewController(baristaScene, animated: true)
        }else{
            let categoryScene =  categoryVC.instantiate(fromAppStoryboard: .Category)
            categoryScene.categoryName = self.categories[indexPath.row].name
            categoryScene.categoryId =  String(self.categories[indexPath.row].id)
            categoryScene.categoryImage =  self.categories[indexPath.row].image
              categoryScene.type = "category"
            self.navigationController?.pushViewController(categoryScene, animated: true)
        }
    }
    
    
}
