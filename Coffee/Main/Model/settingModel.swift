//
//  settingModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - settingModel
struct settingModel: Codable {
    let arName, enName: String
    let items: [Item]
}

// MARK: - Item
struct Item: Codable {
    let artextName, entextName, imageName: String
}


