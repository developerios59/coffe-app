//
//  categoryModel.swift
//  Coffee
//
//  Created by Mustafa on 10/8/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - categoryModel
struct categoryModel: Codable  ,CodableInit{
    let value, key: String
    let data: [categoryData]
}

// MARK: - categoryData
struct categoryData: Codable ,CodableInit{
    let id: Int
    let name: String
    let image: String
}
