
//
//  whatsAppModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 9/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - whatsAppModel
struct whatsAppModel: Codable ,CodableInit{
    let value, key:String
    let msg,data: String?
}
