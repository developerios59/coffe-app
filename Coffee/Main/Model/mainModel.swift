//
//  mainModel.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - mainModel
struct mainModel: Codable ,CodableInit{
    let value, key: String
    let sliders: [String]
    let categories, brands: [Brand]
}

// MARK: - Brand
struct Brand: Codable,CodableInit{
    let id: Int
    let name: String
    let image: String
}

