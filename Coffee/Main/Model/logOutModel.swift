//
//  logOutModel.swift
//  Mazad
//
//  Created by saleh on 2/5/20.
//  Copyright © 2020 saleh. All rights reserved.
//

import Foundation

// MARK: - Welcome
struct logOutModel: Codable,CodableInit {
    let key, value: String
    let msg,data: String?
}

