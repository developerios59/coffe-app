//
//  categoryCell.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class categoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryNameLB: UILabel!
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        categoryImg.layer.cornerRadius = categoryImg.frame.height / 2
            categoryImg.clipsToBounds = true
       
    }

    
    func configureCell(data:Brand) {
        categoryImg.kf.setImage(with: URL(string: data.image))
        categoryNameLB.text = data.name
        
    }
}
