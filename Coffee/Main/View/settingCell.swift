//
//  settingCell.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH

class settingCell: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var nameLB: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        arrowImg.image = UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "noun_left_arrow"  : "eft" )
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(data:Item) {
        img.image = UIImage(named: data.imageName)
        
        nameLB.text = MOLHLanguage.currentAppleLanguage().contains("en") ? data.entextName : data.artextName
    }

}
