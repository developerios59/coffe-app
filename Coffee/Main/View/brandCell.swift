//
//  brandCell.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class brandCell: UICollectionViewCell {

    @IBOutlet weak var brandImg: UIImageViewX!
    @IBOutlet weak var brandNameLB: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.brandImg.layer.cornerRadius  = 10
        brandImg.clipsToBounds = true
    }
    
    
   func configureCell(data:Brand) {
        brandImg.kf.setImage(with: URL(string: data.image))
        brandNameLB.text = data.name
    
    }
    
}
