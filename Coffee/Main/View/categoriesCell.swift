//
//  categoriesCell.swift
//  Coffee
//
//  Created by Mustafa on 10/8/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class categoriesCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryNameLB: UILabel!
    
    override func layoutSubviews() {
        super.layoutSubviews()
        categoryImg.layer.cornerRadius = categoryImg.frame.width / 2
    }
    
    func configureCell(data:categoryData) {
        categoryImg.kf.setImage(with: URL(string: data.image))
        categoryNameLB.text = data.name
    }

}
