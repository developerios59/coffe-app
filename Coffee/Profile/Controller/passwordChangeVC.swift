//
//  passwordChangeVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class passwordChangeVC: UIViewController, NVActivityIndicatorViewable ,UITextFieldDelegate{

    @IBOutlet weak var oldPassTF: UITextFieldX!
    @IBOutlet weak var newPassTF: UITextFieldX!
    @IBOutlet weak var confirmPassTF: UITextFieldX!
    @IBOutlet weak var containerView: UIViewX!
    
    let userID = Helper.standard.userId()
    
    override func viewDidLoad() {
        super.viewDidLoad()
          containerView.layer.masksToBounds = true
          oldPassTF.delegate = self
          newPassTF.delegate = self
          confirmPassTF.delegate = self
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    func resetPassword() {
        
        let passwordMatched = newPassTF.text == confirmPassTF.text ? "checked" : ""
        
        let validation = Validation.shared.validate(values: (ValidationType.password, oldPassTF.text!),(ValidationType.password,newPassTF.text!),(ValidationType.confirmPassword,confirmPassTF.text!),(ValidationType.passwordsMatched, passwordMatched))
        
        switch validation {
        case .success:
            break
        case .failure(_, let massage):
            Helper.standard.displayAlert(title: "Error".localized, message: massage, vc: self)
            return
        }
        
      
        
        var resPassResponse: HandleResponse<passwordChangeModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message:model.msg, vc: self!)
                    }else if model.value == "1" {
                        let alert = UIAlertController(title: "Success".localized, message: "your password is changed".localized, preferredStyle: .alert)
                        
                        alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: { (_) in
                            self?.dismiss(animated: true, completion: nil)
                        }))
                        
                        self?.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.resetPass( current_password: oldPassTF.text!, password: newPassTF.text!, lang: "en".localized).send(passwordChangeModel.self, then: resPassResponse)
            
        }else{
             Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
            
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        resetPassword()
        return true
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didSelectConfirmBu(_ sender: Any) {
        resetPassword()
    }
}
