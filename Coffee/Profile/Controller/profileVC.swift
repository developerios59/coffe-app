//
//  profileVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class profileVC: UIViewController ,NVActivityIndicatorViewable,UITextFieldDelegate{
    
    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var nameTF: UITextFieldX!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var mobileNumTF: UITextFieldX!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var profileIMg: UIImageView!
    
    let userId = Helper.standard.userId()

    var address = ""
    var latitude = ""
    var longtitude = ""
    
    var isProfileEdit = false
    
    var profileResponse: HandleResponse<signModel> {
        return {[weak self] (response) in
            self?.stopAnimating()
            switch response {
            case .failure(let error):
                // TODO: - Handle error as you want, printing isn't handling.
                print(error)
            case .success(let model):
                  print(model)
                if model.value == "0" {
                    Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                }else if model.value == "1" {
                    
                    self?.nameTF.placeholder = model.data!.name
                    self?.mobileNumTF.placeholder = model.data!.phone
                    self?.emailTF.placeholder = model.data!.email
                    self?.userImg.kf.setImage(with: URL(string: model.data!.avatar))
                   self?.nameTF.text = ""
                   self?.mobileNumTF.text = ""
                   self?.emailTF.text = ""
                    UserDefaults.standard.set(model.data?.avatar, forKey: "avatar")
                    UserDefaults.standard.set(model.data?.name, forKey: "userName")
                    UserDefaults.standard.set(model.data?.email, forKey: "email")
                    UserDefaults.standard.set(model.data?.phone, forKey: "userMobileNumber")
                    if self!.isProfileEdit {
                    Helper.standard.displayAlert(title: "Success".localized, message: "your data is edited successfully".localized, vc: self!)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       setupUI()
     
        let imagetap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        let profiletap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        userImg.addGestureRecognizer(imagetap)
        profileIMg.addGestureRecognizer(profiletap)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    override func viewWillAppear(_ animated: Bool) {
          getProfileData(phone: "", email: "", name: "")
      }
      
    
    
    func setupUI() {
        nameTF.delegate = self
        mobileNumTF.delegate = self
        emailTF.delegate = self
       
          backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        containerView.layer.cornerRadius = 15
        containerView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        
        userImg.layer.cornerRadius = userImg.frame.height / 2
    }
    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func getProfileData(phone:String,email:String,name:String) {
        // Api Request
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.ProfileEdit(lang: "en".localized, phone: phone, name: name, email: email, address: self.address, lat: self.latitude, lng: self.longtitude).send(signModel.self, then: profileResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        self.view.endEditing(true)
        AttachmentHandler.shared.showAttachmentActionSheet(vc: self)
        AttachmentHandler.shared.imagePickedBlock = { (image) in
            self.userImg.layer.cornerRadius = 33
            self.userImg.image = image
            let profileImgData = image.jpegData(compressionQuality: 0.5)!

            let uploadData = [UploadData(data: profileImgData, fileName: "image", mimeType: "image/png", name: "avatar")]

            if Reachability.isConnectedToNetwork(){
                self.startAnimating()
                self.isProfileEdit = true
                UserRouter.ProfileEdit(lang: "en", phone: "", name: "", email: "", address: "", lat: "", lng: "").send(signModel.self, data: uploadData, progress: nil) { (response) in
                    self.stopAnimating()
                    switch response {
                    case .failure(let error):
                        // TODO: - Handle error as you want, printing isn't handling.
                        print(error)
                    case .success(let model):
                        print(model)
                        if model.value == "0" {
                            Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self)
                        }else if model.value == "1" {
                            UserDefaults.standard.set(model.data?.avatar, forKey: "avatar")
                            Helper.standard.displayAlert(title: "Success".localized, message: "your data is edited successfully".localized, vc: self)
                        }
                    }
                }
            }else{
                Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
            }
        }
    }
    
   

    @IBAction func didSelectPasswordEditBu(_ sender: Any) {
       let passwordChangeScene =  passwordChangeVC.instantiate(fromAppStoryboard: .Profile)
        self.present(passwordChangeScene, animated: true, completion: nil)
    }
    
    
    
        
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
               editSave()
               return true
           }
            
        
        @IBAction func didSelectSaveBu(_ sender: Any) {
            editSave()
        }
        
        func editSave() {
           if nameTF.text!.isEmpty && mobileNumTF.text!.isEmpty &&   emailTF.text!.isEmpty{
                Helper.standard.displayAlert(title: "Error".localized, message: "All fields is Empty".localized, vc: self)
                return
            }
            var  response : Valid!
            
            if !mobileNumTF.text!.isEmpty {
            response =  Validation.shared.validate(values: (ValidationType.phoneNo , mobileNumTF.text!))
            }
                
            if !emailTF.text!.isEmpty {
                 response =  Validation.shared.validate(values: (ValidationType.email , emailTF.text!))
            }
            
            switch response {
            case .success:
                break
            case .failure(_, let message):
                Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
                return
            case .none:
                break
            }
            
          
            if Reachability.isConnectedToNetwork(){
                self.startAnimating()
                self.isProfileEdit = true
                UserRouter.ProfileEdit(lang: "en".localized, phone: mobileNumTF.text!, name: nameTF.text!, email: emailTF.text!, address: address, lat: latitude, lng: longtitude).send(signModel.self, then: profileResponse)
            }else{
                Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
            }
        }
        
        
    }

