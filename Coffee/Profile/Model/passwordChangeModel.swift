//
//  passwordChangeModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - Welcome
struct passwordChangeModel: Codable ,CodableInit{
    let value, key: String
    let msg: String
    let data: Int?
}

