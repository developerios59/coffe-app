//
//  AppDelegate.swift
//  Coffee
//
//  Created by Mustafa on 9/29/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import MOLH
import DropDown
import Kingfisher
import Firebase
import FirebaseMessaging
import GoogleMaps
import NVActivityIndicatorView
import Siren

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate ,MOLHResetable{
   
    var window: UIWindow?
    
let mainStoryboard = AppStoryboard.Main.instance
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Thread.sleep(forTimeInterval: 1.0)
        
        let googleApiKey = "AIzaSyBBq6guAruqDAqhxbTNIJIV-pQsxVmpDcY"
                     GMSServices.provideAPIKey(googleApiKey)
          
           
            FirebaseApp.configure()
           
           let pushManager = PushNotificationManager()
               pushManager.registerForPushNotifications()
          
           IQKeyboardManager.shared.enable = true
           hyperCriticalRulesExample()
           
           NVActivityIndicatorView.DEFAULT_TYPE = .circleStrokeSpin
           NVActivityIndicatorView.DEFAULT_COLOR = .black
           NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = .white
        
//        if let statusBar = UIApplication.shared.value(forKey: "statusBar") as? UIView {
//               statusBar.backgroundColor = UIColor.white
//           }
        
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.font: UIFont(name: "JF Flat", size: 10)], for: .normal)
        
            MOLH.shared.activate(true)
            UserDefaults.standard.set(MOLHLanguage.currentAppleLanguage(), forKey: "lang")
           
         let mainScene = languageChooseVC.instantiate(fromAppStoryboard: .authentication)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = mainScene
        self.window?.makeKeyAndVisible()
        return true
    }

    func reset() {
        let mainScene = mainStoryboard.instantiateViewController(withIdentifier: "mainTabBar")
        
       self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = mainScene
        self.window?.makeKeyAndVisible()
    }
    
    
    func hyperCriticalRulesExample() {
          let siren = Siren.shared
          siren.rulesManager = RulesManager(globalRules: .critical,
                                            showAlertAfterCurrentVersionHasBeenReleasedForDays: 0)

          siren.wail { results in
              switch results {
              case .success(let updateResults):
                  print("AlertAction ", updateResults.alertAction)
                  print("Localization ", updateResults.localization)
                  print("Model ", updateResults.model)
                  print("UpdateType ", updateResults.updateType)
              case .failure(let error):
                  print(error.localizedDescription)
              }
          }
      }

    
}

