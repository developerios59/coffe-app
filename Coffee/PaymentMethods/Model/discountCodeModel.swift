//
//  discountCodeModel.swift
//  Coffee
//
//  Created by Mustafa on 10/21/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - discountCodeModel
struct discountCodeModel: Codable ,CodableInit{
    let value, key: String
    let msg:String?
    let totalOrder, data: Double?
    let discount: String?

    enum CodingKeys: String, CodingKey {
        case value, key,msg
        case totalOrder = "total_order"
        case data, discount
    }
}

