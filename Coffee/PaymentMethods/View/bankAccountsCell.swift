//
//  bankAccountsCell.swift
//  goldenBrickAgent
//
//  Created by Mustafa on 10/4/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class bankAccountsCell: UITableViewCell {

    @IBOutlet weak var bankNameLB: UILabel!
    @IBOutlet weak var accountNameLB: UILabel!
    @IBOutlet weak var accountNumberLB: UILabel!
    @IBOutlet weak var ibanNumberLB: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
//    func configureCell(data:bankTransferData) {
//        self.bankNameLB.text = data.nameBank
//        self.accountNameLB.text = data.accountName
//        self.accountNumberLB.text = data.accountNumber
//        self.ibanNumberLB.text = data.ibanNumber
//    }

}
