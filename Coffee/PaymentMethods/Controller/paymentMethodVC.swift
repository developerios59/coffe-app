//
//  paymentMethodVC.swift
//  Coffee
//
//  Created by Mustafa on 10/6/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView
import DLRadioButton
import BEMCheckBox
import RealmSwift
import WebKit
class paymentMethodVC: UIViewController ,NVActivityIndicatorViewable {
    
    
    @IBOutlet weak var onlineBtn: DLRadioButton!
    @IBOutlet weak var madaBtn: DLRadioButton!
    @IBOutlet weak var cashBtn: DLRadioButton!
    @IBOutlet weak var discountTF: UITextField!
    @IBOutlet weak var totalPriceLB: UILabel!
    @IBOutlet weak var errorLB: UILabel!
    @IBOutlet weak var backBTn: UIButton!
    
    
    
    let userDefaults = UserDefaults.standard
    var deliveryFromDefualt = ""
    var totalPriceFromDefualt =  ""
    var taxsFromDefualt =  ""
    
    let userId = Helper.standard.userId()
    var orderId = ""
    var paymentType = "cash"
    
    var orderdCart = ""
    var time = ""
    var date = ""
    var address = ""
    var lat = ""
    var lng = ""
    var delivery = 0
    var tax = ""
    var total = 0
    var cityID = ""
    var userName = ""
    var phoneNumber = ""
    var discount = ""
    var paymentMethodId = ""
    var totalRequired = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        deliveryFromDefualt = userDefaults.string(forKey: "delivery") ?? ""
        totalPriceFromDefualt = userDefaults.string(forKey: "totalToOrder") ?? ""
        taxsFromDefualt = userDefaults.string(forKey: "taxToOrder") ?? ""
        setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setupUI() {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        self.totalRequired = "\(total + delivery)"
        self.cashBtn.isSelected = true
        self.discountTF.delegate = self
        
        backBTn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        madaBtn.setTitle("Mada payment".localized, for: .normal)
        onlineBtn.setTitle("Cash payment".localized, for: .normal)
        
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didSelectOnlinePaymentBu(_ sender: Any) {
        self.paymentType = "visa"
        self.paymentMethodId = "4"
        loadWebView(paymentType: paymentType)
        
    }
    
    @IBAction func madaOnlinePaymentBu(_ sender: Any) {
        self.paymentType = "mada"
        self.paymentMethodId = "4"
        loadWebView(paymentType: paymentType)
            }
    
    @IBAction func didSelectCashPaymentBu(_ sender: Any) {
        self.paymentMethodId = "1"
        self.paymentType = "cash"
    }
    
    
    func checkDiscountCode(code:String,total:Double) {
        if Reachability.isConnectedToNetwork() {
            
            
          
            
            self.startAnimating()
            UserRouter.discountCodecheck(lang: "en".localized, coupon: code, total: String(total)).send(discountCodeModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        self.discount = ""
                        self.totalRequired = "\(self.total + self.delivery)"
                        Helper.standard.displayAlert(title: "Error", message: model.msg!, vc: self)
                    }else if model.value == "1" {
                        let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: String(model.totalOrder!) + " SAR".localized)
                        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 1, range: NSMakeRange(0, attributeString.length))
                        self.errorLB.text  = String(model.data!) + " SAR".localized
                        self.totalPriceLB.attributedText = attributeString
                        self.discount = code
                        self.totalRequired = "\(model.data! + Double(Int(self.delivery)))"
                    }else if model.value == "2" {
                        self.errorLB.text = model.msg
                        self.totalPriceLB.text  = ""
                        self.discount = ""
                        self.totalRequired = "\(self.total + self.delivery)"
                    }
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    func loadWebView(paymentType:String){
        
        let vc = webViewVC()
        vc.paymentMethod = paymentType
        let intValue = Int(Float(totalPriceFromDefualt)!)
        vc.totla = intValue
        self.present(vc, animated: true , completion: nil)
        
    }
    
    @IBAction func didSelectConfirmBu(_ sender: Any) {
        
        CoffeeClient.creatShipment(customerName: self.userName, phoneNumber: self.phoneNumber , deliveryPrice: "\(self.delivery)" , customerAddress: self.address, lat:self.lat, long: self.lng, cityID: "\(self.cityID)", paymentMethodId: self.paymentMethodId)
        
        addOrder(payment: self.paymentType, total: totalPriceFromDefualt, delivery: self.deliveryFromDefualt, tax: taxsFromDefualt, cart: orderdCart, date: date, time: time, address: address, lat: lat, lng: lng)
    }
    
    
    
    func addOrder(payment:String , total:String,delivery:String,tax:String,cart:String,date:String,time:String,address:String,lat:String,lng:String) {
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.addOrder(lang: "en".localized, address: self.address, lat: lat, lng: lng, time: time, date: date, payment: payment, total: total, delivery_price: delivery, tax: tax, carts: cart, coupon: discount).send(orderAddModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self)
                    }else if model.value == "1" {
                        
                        do {
                            let realm = try Realm()
                            try realm.write {
                                let objectsToDelete = realm.objects(CartItem.self)
                                realm.delete(objectsToDelete)
                            }
                        }catch {
                            print(error)
                        }
                        
                        let successProcessScene = successProcessVC.instantiate(fromAppStoryboard: .SuccessProcess)
                        
                        self.navigationController?.pushViewController(successProcessScene, animated: true)
                    }
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
}



extension paymentMethodVC : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if !textField.text!.isEmpty {
            checkDiscountCode(code: textField.text!, total: Double(self.total))
        }else{
            self.errorLB.text = ""
            self.totalPriceLB.text  =  ""
            self.totalRequired = "\(self.total + self.delivery)"
            self.discount = ""
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if !textField.text!.isEmpty {
            checkDiscountCode(code: textField.text!, total: Double(self.total))
        }else{
            
            self.errorLB.text = ""
            self.totalPriceLB.text  =  ""
            self.discount = ""
            self.totalRequired = "\(self.total + self.delivery)"
        }
    }
}

class webViewVC :UIViewController, WKNavigationDelegate {
    
    var webView : WKWebView!
    var totla = 0
    var paymentMethod = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        webView = WKWebView()
        webView.navigationDelegate = self
        let url = URL(string: "https://coffeee.aait-sa.com/online-pay?price=\(self.totla)&methods=\(paymentMethod)")!
        webView.load(URLRequest(url: url))
        webView.allowsBackForwardNavigationGestures = true
        view = webView
    }
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        if let url = webView.url?.absoluteString{
            print("url = \(url)")
            if url.contains("check-pay"){
                self.dismiss(animated: true, completion: nil)
            }
        }
    }
    
    private func webView(webView: WKWebView, didFinishNavigation navigation: WKNavigation!) {
        print("finish to load")
        if let url = webView.url?.absoluteString{
            print("url = \(url)")
        }
    }
}
