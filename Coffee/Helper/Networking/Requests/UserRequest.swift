//
//  ProjectRouter.swift
//  SwiftCairo-App
//
//  Created by abdelrahman mohamed on 4/21/18.
//  Copyright © 2018 abdelrahman mohamed. All rights reserved.
//

import Foundation
import Alamofire

enum UserRouter: URLRequestBuilder {
    
    
    //   REGESTERATION
    case SignUp(phone:String,name:String,email:String,address:String,lat:String
        ,lng:String,password:String,device_id:String,device_type:String,lang:String)
    
    case signIn(phone:String,password:String,device_id:String,device_type:String,lang:String)
    
    case checkCode(code:String,lang:String)
    
    case resendCode(lang:String)
    
    case resetPass(current_password:String,password:String,lang:String)
    
    case forgetPass(phone:String,lang:String)
    
    case updatePass(code:String,password:String,lang:String)
    
    case logOut(device_id:String,device_type:String,lang:String)
    
    
    case companyRegister(lang:String,name:String,phone:String,email:String,category_id:String,notes:String)
    
    // Profile Data
       case ProfileEdit(lang:String,phone:String,name:String,email:String,address:String,lat:String,lng:String)
       
    
    //   APP Data
    case about(lang:String)
    
    case terms(lang:String)
    
    case whatsApp
    
    case costs
    
     case contactUs(lang:String,name:String,phone:String,message:String)
    
     case followUs
    
    //     Main App
    
    case home(lang:String)
    
    case products(lang:String,type_id:String,type:String)
    
    case comments(lang:String,product_id:String)
    
    case addComment(lang:String,comment:String,product_id:String,rate:String)
    
    case productDetails(lang:String,product_id:String)
    
    case categories(lang:String)
    
    case brands(lang:String)
    
    case barista(lang:String)
    
    case baristaDetails(lang:String,barista_id:String)
    
    case favorite(lang:String,product_id:String)
    
    case favorites(lang:String)
    
    case addOrder(lang:String,address:String,lat:String,lng:String,time:String,date:String,payment:String,total:String,delivery_price:String,tax:String,carts:String,coupon:String)
    
    case discountCodecheck(lang:String,coupon:String,total:String)
    
    case filterProducts(lang:String,type_id:String,type:String,price:String)
    
    // Orders
    
    case  orders(lang: String, status: String)
    
    case orderProducts(lang:String, order_id: String)
    
    case similarProducts(lang:String,product_id:String)
    
    case orderTrack(lang:String,order_id:String,status:String,confirm:String)
    
    case orderRate(lang:String,order_id:String,rate:String)
    
    
    // NOTIFICATION
           
    case notifications(lang:String)
           
    case deleteNotification(lang:String,notification_id:String)
           
          
    
    
    // MARK: - Path
    internal var path: String {
        switch self {
            
        case .SignUp:
            return "sign-up"
        case .signIn:
            return "sign-in"
        case .checkCode:
            return "check-code"
        case .resendCode:
            return "resend-code"
        case .resetPass:
            return "reset-password"
        case .forgetPass:
            return "forget-password"
        case .updatePass:
            return "update-password"
        case .logOut:
            return "log-out"
            
        case .home:
            return "home"
        case .products:
            return "get-products"
        case .productDetails:
            return "details-product"
            
            
        case .brands:
            return "brands"
        case .categories:
            return "categories"
        case .barista:
            return "baristas"
        case .orders:
            return "user-orders"
        case .orderProducts:
            return "details-order"
        case .favorite:
            return "favorite"
        case .favorites:
            return "user-favorites"
            
        case .whatsApp:
            return "contact-whatsapp"
        case .comments:
            return "product-comments"
        case .addComment:
            return "add-comment"
            
        case .baristaDetails:
            return "get-products-barista"
        case .addOrder:
            return "add-order"
        case .costs:
            return "costs"
        case .similarProducts:
            return "similar-products"
        case .orderTrack:
            return "track-order"
        case .ProfileEdit:
            return "edit-profile"
            
        case .notifications:
            return "notifications"
        case .deleteNotification:
            return "delete-notification"
        case .about:
            return "about"
        case .terms:
            return "terms"
        case .contactUs:
            return "contact-us"
        case .followUs:
            return "follow-us"
        case .companyRegister:
            return "companies-registration"
        case .discountCodecheck:
            return "discount"
        case .orderRate:
             return "evaluations"
        case .filterProducts:
            return "filter"
        }
    }
    
    // MARK: - Parameters
    internal var parameters: Parameters? {
        switch self {
        case .SignUp(let phone, let name, let email, let address, let lat, let lng, let password, let device_id, let device_type , let lang):
            return ["phone":phone,
                    "name":name,
                    "email":email,
                    "address":address,
                    "lat":lat,
                    "lng":lng,
                    "password":password,
                    "device_id":device_id,
                    "device_type":device_type,
                    "lang":lang]
        case .signIn(let phone, let password, let device_id, let device_type, let lang):
            return ["phone":phone,
                    "password":password,
                    "device_id":device_id,
                    "device_type":device_type,
                    "lang":lang]
        case .checkCode( let code, let lang):
            return ["code":code,
                    "lang":lang]
        case .resendCode( let lang):
            return ["lang":lang]
        case .resetPass( let current_password, let password, let lang):
            return ["current_password":current_password,
                    "password":password,
                    "lang":lang]
        case .forgetPass(let phone, let lang):
            return ["phone":phone,
                    "lang":lang]
        case .updatePass(let code , let password, let lang):
            return ["code":code,
                    "password":password,
                    "lang":lang]
        case .logOut( let device_id, let device_type, let lang):
            return ["device_id":device_id,
                    "device_type":device_type,
                    "lang":lang]
            
            
            
        case .home( let lang):
            return ["lang":lang]
        case .products(let lang,  let type_id,  let type):
            return ["lang":lang,
                    "type_id":type_id,
                    "type":type]
            
        case .productDetails(let lang, let product_id):
            return ["lang":lang,
                    "product_id":product_id]
            
            
        case .brands( let lang):
            return ["lang":lang]
        case .categories( let lang):
            return ["lang":lang]
        case .barista( let lang):
            return ["lang":lang]
        case .orders(let lang, let status):
            return ["lang":lang,
                    "status":status]
            
        case .orderProducts(let  lang,let order_id):
            return ["lang":lang,
                    "order_id":order_id]
        case .favorite( let lang,  let product_id):
            return ["lang":lang,
                    "product_id":product_id]
        case .favorites( let lang):
            return ["lang":lang]
        case .whatsApp:
            return nil
            
        case .comments( let lang,  let product_id):
            return ["lang":lang,
                    "product_id":product_id]
            
        case .addComment(let lang,let comment,let product_id,let rate):
            return ["lang":lang,
                    "comment":comment,
                    "product_id":product_id,
                    "rate":rate]
        case .baristaDetails( let lang,  let barista_id):
            return ["lang":lang,
                    "barista_id":barista_id]
            
        case .addOrder( let lang,  let address,  let lat,  let lng,  let time,  let date,  let payment,  let total,  let delivery_price,  let tax,  let carts,let coupon ):
            
            print(["lang":lang,
                   "address":address,
                   "lat":lat,
                   "lng":lng,
                   "time":time,
                   "date":date,
                   "payment":payment,
                   "total":total,
                   "delivery_price":delivery_price,
                   "tax":tax,
                   "carts":carts,
                   "coupon":coupon])
            return ["lang":lang,
                    "address":address,
                    "lat":lat,
                    "lng":lng,
                    "time":time,
                    "date":date,
                    "payment":payment,
                    "total":total,
                    "delivery_price":delivery_price,
                    "tax":tax,
                    "carts":carts,
                    "coupon":coupon]
        case .costs:
            return nil
        case .similarProducts( let lang,  let product_id):
            return ["lang":lang,
                    "product_id" : product_id]
        case .orderTrack(let lang,  let order_id,let status ,let confirm):
            return ["lang":lang,
                    "order_id":order_id,
                    "status":status,
                    "confirm":confirm]
        case .ProfileEdit( let lang,  let phone,  let name,  let email, let address,  let lat,  let lng):
            
            return [ "lang":lang,
                     "phone":phone,
                     "name":name,
                     "email":email,
                     "lat":lat,
                     "lng":lng,
                     "address":address ]
            
        case .notifications(let lang):
            return ["lang":lang]
        case .deleteNotification(let lang, let notification_id):
            return ["lang":lang,
                    "notification_id":notification_id]
            
        case .about(let lang):
            return ["lang":lang]
        case .terms(let lang):
            return ["lang":lang]
        case .followUs:
            return nil
            
        case .contactUs(let lang, let name,let phone, let message):
            return ["lang":lang,
                    "name":name,
                    "phone":phone,
                    "message":message  ]
        case .companyRegister( let lang, let name,  let phone,  let email,  let category_id,  let notes):
            return ["lang":lang,
                    "name":name,
                    "phone":phone,
                    "email":email,
                    "category_id":category_id,
                    "notes":notes]
            
        case .discountCodecheck( let lang,  let coupon,  let total):
             return ["lang":lang,
                     "coupon":coupon,
                     "total":total]
        case .orderRate( let lang,  let order_id,  let rate):
             return ["lang":lang,
                     "order_id":order_id,
                     "rate":rate]
        case .filterProducts( let lang,  let type_id,  let type, let price):
            return ["lang":lang,
                    "type_id":type_id,
                    "type":type,
                    "price":price]
        }
    }
    
    // MARK: - Methods
    internal var method: HTTPMethod {
        return .post
    }
    
}



