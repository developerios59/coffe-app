//
//  authKey.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

class AuthManager {
    
  static let standard = AuthManager()
    
    static func authKey() -> String {
        
        if let token =  UserDefaults.standard.value(forKey: "ToKen")  as? String{
            return token
        }
        
        return ""
        
    }
}
