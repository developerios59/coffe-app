//
//  PushNotificationManager.swift
//  FirebaseStarterKit
//
//  Created by Florian Marcu on 1/28/19.
//  Copyright © 2019 Instamobile. All rights reserved.
//

import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging

class PushNotificationManager: NSObject, UNUserNotificationCenterDelegate,MessagingDelegate  {
    
    
    func registerForPushNotifications() {
        
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().delegate = self
//        Messaging.messaging().shouldEstablishDirectChannel = true

        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions,completionHandler: {_, _ in })
            // For iOS 10 data message (sent via FCM)
               Messaging.messaging().delegate = self
        }else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func PushTokenIfNeeded() -> String {
        if let token = Messaging.messaging().fcmToken {
             UserDefaults.standard.set(token, forKey: "deviceID")
            return token
        }else{
            return "12"
        }
    }
    
    
  
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        print("fcmToken:- \(fcmToken) ")
//        print("SecondfcmToken:- \(messaging.fcmToken) ")
    //    UserDefaults.standard.set(fcmToken, forKey: "deviceID")
    }
    

    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
            print("RESPONSE\(response)")
        
        let userInfo = response.notification.request.content.userInfo

        let targetValue = userInfo[AnyHashable("type")] as? String
        print(userInfo)
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
         //print(targetValue)


        if targetValue == "notification" {
              let orderProductsScene = ordersProductsVC.instantiate(fromAppStoryboard: .Orders)
            if  let tabBarController = UIApplication.shared.windows.first?.rootViewController as? UITabBarController,
                let navController = tabBarController.selectedViewController as? UINavigationController ,
                let orderId = userInfo[AnyHashable("order_id")] as? String{
                 orderProductsScene.orderId = orderId
                navController.pushViewController(orderProductsScene, animated: true)
            }
        }
        
        completionHandler()
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[AnyHashable("offers")] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
       
        let userInfo = notification.request.content.userInfo
        
        let targetValue = userInfo[AnyHashable("type")] as? String
        
         print(userInfo)
//       print(targetValue)
//        if targetValue == "block" || targetValue == "user_deleted" {
//            let rootviewcontroller: UIWindow = ((UIApplication.shared.delegate?.window)!)!
//            
//            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Sign")
//            rootviewcontroller.rootViewController = vc
//                UIApplication.shared.windows.first?.rootViewController = vc
//            
//        }
        
        completionHandler([.alert,.badge, .sound])
    }
    
    
}
