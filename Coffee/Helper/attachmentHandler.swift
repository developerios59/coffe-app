//
//  attachmentHandler.swift
//  shalihatBasic
//
//  Created by Mustafa Abdein on 10/8/19.
//  Copyright © 2019 Mustafa Abdein. All rights reserved.
//



import Foundation
import UIKit
import MobileCoreServices
import AVFoundation
import Photos







class AttachmentHandler: NSObject{
    static let shared = AttachmentHandler()
    fileprivate var currentVC: UIViewController?
    
    //MARK: - Internal Properties
    var imagePickedBlock: ((UIImage) -> Void)?
    var videoPickedBlock: ((NSURL) -> Void)?
    var filePickedBlock: ((URL) -> Void)?
    
    
    enum AttachmentType: String{
        case camera, video, photoLibrary
    }
    
    
   //MARK: - Constants
    struct Constants {
        static let actionFileTypeHeading = "Add a File".localized
        static let actionFileTypeDescription = "Choose a filetype to add...".localized
        static let camera = "Camera".localized
        static let phoneLibrary = "Photo Library".localized
        static let video = "Video"
        static let file = "File".localized
        
        
        static let alertForPhotoLibraryMessage = "App does not have access to your photos. To enable access, tap settings and turn on Photo Library Access.".localized
        
        static let alertForCameraAccessMessage = "App does not have access to your camera. To enable access, tap settings and turn on Camera.".localized
        
        static let alertForVideoLibraryMessage = "App does not have access to your video. To enable access, tap settings and turn on Video Library Access.".localized
        
        
        static let settingsBtnTitle = "Settings".localized
        static let cancelBtnTitle = "Cancel".localized
        
    }
    
    //MARK: - showAttachmentActionSheet
       // This function is used to show the attachment sheet for image, video, photo and file.
       func showAttachmentActionSheet(vc: UIViewController) {
           currentVC = vc
          
           let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
           
           actionSheet.addAction(UIAlertAction(title: Constants.camera, style: .default, handler: { (action) -> Void in
               self.authorisationStatus(attachmentTypeEnum: .camera, vc: self.currentVC!)
           }))
           
           actionSheet.addAction(UIAlertAction(title: Constants.phoneLibrary, style: .default, handler: { (action) -> Void in
               self.authorisationStatus(attachmentTypeEnum: .photoLibrary, vc: self.currentVC!)
           }))
           
           
           
           actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
           
           vc.present(actionSheet, animated: true, completion: nil)
           }
       
       
    
    
    func showFileAttachmentActionSheet(vc: UIViewController) {
            currentVC = vc
         let actionSheet = UIAlertController(title: Constants.actionFileTypeHeading, message: Constants.actionFileTypeDescription, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: Constants.file, style: .default, handler: { (action) -> Void in
                   self.documentPicker()
               }))
         actionSheet.addAction(UIAlertAction(title: Constants.cancelBtnTitle, style: .cancel, handler: nil))
        
        vc.present(actionSheet, animated: true, completion: nil)
        
    }
    
    //MARK: - Authorisation Status
    // This is used to check the authorisation status whether user gives access to import the image, photo library, video.
    // if the user gives access, then we can import the data safely
    // if not show them alert to access from settings.
    func authorisationStatus(attachmentTypeEnum: AttachmentType, vc: UIViewController){
            currentVC = vc
             
            let status = PHPhotoLibrary.authorizationStatus()
           
            switch status {
            case .authorized:
                if attachmentTypeEnum == AttachmentType.camera{
                    self.openCamera()
                }
                if attachmentTypeEnum == AttachmentType.photoLibrary{
                    self.photoLibrary()
                }
                
            case .denied:
                print("permission denied")
                self.addAlertForSettings(attachmentTypeEnum)
            case .notDetermined:
                print("Permission Not Determined")
                
            
                if attachmentTypeEnum == AttachmentType.camera {
                    AVCaptureDevice.requestAccess(for: AVMediaType.video) { response in
                        if response {
                            self.openCamera()
                        } else {
                            DispatchQueue.main.async {
                                self.addAlertForSettings(attachmentTypeEnum)
                            }
                        }
                    }
                }else if  attachmentTypeEnum == AttachmentType.photoLibrary{
                    
                    PHPhotoLibrary.requestAuthorization({ (status) in
                        
                        if status == PHAuthorizationStatus.authorized {
                            self.photoLibrary()
                        }else{
                            DispatchQueue.main.async {
                                self.addAlertForSettings(attachmentTypeEnum)
                            }
                             
                        }
                    })
                    
                }
                
                
            case .restricted:
                print("permission restricted")
                self.addAlertForSettings(attachmentTypeEnum)
            default:
                break
            }
            
        }
    
    
    //MARK: - CAMERA PICKER
    //This function is used to open camera from the iphone and
    func openCamera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
             DispatchQueue.main.async {
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self
            myPickerController.sourceType = .camera
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            }
        }
    }
    
    
    //MARK: - PHOTO PICKER
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            DispatchQueue.main.async {
                 let myPickerController = UIImagePickerController()
                           myPickerController.delegate = self
                           myPickerController.sourceType = .photoLibrary
                self.currentVC?.present(myPickerController, animated: true, completion: nil)
            
            }
        }
    }
    

    
    //MARK: - FILE PICKER
    func documentPicker(){
 DispatchQueue.main.async {
        let importMenu = UIDocumentMenuViewController(documentTypes: [String(kUTTypePDF)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
            self.currentVC?.present(importMenu, animated: true, completion: nil)
        }
    }
    
    //MARK: - SETTINGS ALERT
    func addAlertForSettings(_ attachmentTypeEnum: AttachmentType){
        var alertTitle: String = ""
        if attachmentTypeEnum == AttachmentType.camera{
            alertTitle = Constants.alertForCameraAccessMessage
        }
        if attachmentTypeEnum == AttachmentType.photoLibrary{
            alertTitle = Constants.alertForPhotoLibraryMessage
        }
//        if attachmentTypeEnum == AttachmentType.video{
//            alertTitle = Constants.alertForVideoLibraryMessage
//        }
        
        let cameraUnavailableAlertController = UIAlertController (title: alertTitle , message: nil, preferredStyle: .alert)
        
        let settingsAction = UIAlertAction(title: Constants.settingsBtnTitle, style: .destructive) { (_) -> Void in
            let settingsUrl = NSURL(string:UIApplication.openSettingsURLString)
            if let url = settingsUrl {
                UIApplication.shared.open(url as URL, options: [:], completionHandler: nil)
            }
        }
        let cancelAction = UIAlertAction(title: Constants.cancelBtnTitle, style: .default, handler: nil)
        cameraUnavailableAlertController .addAction(cancelAction)
        cameraUnavailableAlertController .addAction(settingsAction)
        currentVC?.present(cameraUnavailableAlertController , animated: true, completion: nil)
        }
    }


//MARK: - IMAGE PICKER DELEGATE
// This is responsible for image picker interface to access image, video and then responsibel for canceling the picker
extension AttachmentHandler: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[.originalImage] as? UIImage {
            self.imagePickedBlock?(image)
        } else{
            print("Something went wrong in  image")
        }

        currentVC?.dismiss(animated: true, completion: nil)
    }
    

}

//MARK: - FILE IMPORT DELEGATE
extension AttachmentHandler: UIDocumentMenuDelegate, UIDocumentPickerDelegate{
    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        currentVC?.present(documentPicker, animated: true, completion: nil)
    }
    
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        print("url", url)
        self.filePickedBlock?(url)
    }
    
    //    Method to handle cancel action.
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        currentVC?.dismiss(animated: true, completion: nil)
    }
    
}
