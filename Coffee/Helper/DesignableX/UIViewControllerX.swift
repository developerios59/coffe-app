//
//  UIViewControllerX.swift
//  shalihatBasic
//
//  Created by Mustafa Abdein on 9/26/19.
//  Copyright © 2019 Mustafa Abdein. All rights reserved.
//

import UIKit

@IBDesignable
class UIViewControllerX: UIViewController {
    
    @IBInspectable var lightStatusBar: Bool = false
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        get {
            if lightStatusBar {
                return UIStatusBarStyle.lightContent
            } else {
                return UIStatusBarStyle.default
            }
        }
    }
}
