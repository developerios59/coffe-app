//
//  customTabBarController.swift
//  Coffee
//
//  Created by Mustafa on 10/11/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH

class customTabBarController: UITabBarController  {
    override var traitCollection: UITraitCollection {
        let realTraits = super.traitCollection
        let fakeTraits = UITraitCollection(horizontalSizeClass: .regular)
        return UITraitCollection(traitsFrom: [realTraits, fakeTraits])
    }
}


