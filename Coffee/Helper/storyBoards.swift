//
//  storyBoards.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/26/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

enum AppStoryboard : String {
    
    case Main
    case authentication
    case Category
    case Brands
    case Products
    case Comments
    case Cart
    case DeliveryInfo
    case PaymentMethods
    case Barista
    case Orders
    case Favourite
    case CompanyRegister
    case OrderFollowUp
    case Profile
    case Notification
    case AppInfo
   case ContactUs
    case SuccessProcess
    
    
    
    
    var instance : UIStoryboard {
        
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T : UIViewController>(viewControllerClass : T.Type, function : String = #function, line : Int = #line, file : String = #file) -> T {
        
        let storyboardID = (viewControllerClass as UIViewController.Type).storyboardID
         
        guard let scene = instance.instantiateViewController(withIdentifier: storyboardID) as? T else {
            
            fatalError("ViewController with identifier \(storyboardID), not found in \(self.rawValue) Storyboard.\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
        }
        
        return scene
    }
    
    func initialViewController() -> UIViewController? {
        
        return instance.instantiateInitialViewController()
    }
}

extension UIViewController {
    
    // Not using static as it wont be possible to override to provide custom storyboardID then
    class var storyboardID : String {
        
        return "\(self)"
    }
    
    static func instantiate(fromAppStoryboard appStoryboard: AppStoryboard) -> Self {
        
        return appStoryboard.viewController(viewControllerClass: self)
    }

}
