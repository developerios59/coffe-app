//
//  File.swift
//  Fahd Hospital
//
//  Created by Sherif Mahmoud on 2/10/19.
//  Copyright © 2019 OrangeSoft. All rights reserved.
//


import UIKit
//import MOLH


class Helper{
    
    static let standard = Helper()
   
   let formatter = DateFormatter()
    let userCalender = Calendar.current
       let requestedComponent : Set<Calendar.Component> = [
          // Calendar.Component.month,
           Calendar.Component.day,
           Calendar.Component.hour,
           Calendar.Component.minute,
           Calendar.Component.second
       ]
    
     func userId() -> String {
        let userId = "\(UserDefaults.standard.value(forKey: "UserId") ?? "")"
        
        print("UserID: \(userId)")
        return userId
    }
    
    func token() -> String {
          let tokenId = "\(UserDefaults.standard.value(forKey: "ToKen") ?? "")"
          
          print("Token: \(tokenId)")
          return tokenId
      }
    
    
    
    func deviceID() -> String {
        let deviceId = "\(UserDefaults.standard.value(forKey: "deviceID") ?? "")"
         print("deviceId: \(deviceId)")
        return deviceId
    }
    
    func displayAlert(title:String,message:String ,vc :UIViewController){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: nil))
        vc.present(alert, animated: true, completion: nil)
    }
    
    func showVisitorPopUp (vc :UIViewController) {
        let visitorScene = visitorPopUpVC.instantiate(fromAppStoryboard: .authentication)
            vc.present(visitorScene, animated: true, completion: nil)
    }


    func timeCalculator(dateFormat: String, endTime: String, startTime: Date = Date()) -> DateComponents {
        formatter.dateFormat = dateFormat
        let _startTime = startTime
        let _endTime = formatter.date(from: endTime)
        let timeDifference = userCalender.dateComponents(requestedComponent, from: _startTime, to: _endTime!)
        return timeDifference
    }
    
 
    
    func hexStringToUIColor (hex:String) -> UIColor {
           var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
           
           if (cString.hasPrefix("#")) {
               cString.remove(at: cString.startIndex)
           }
           
           if ((cString.count) != 6) {
               return UIColor.gray
           }
           
           var rgbValue:UInt64 = 0
           Scanner(string: cString).scanHexInt64(&rgbValue)
           
           return UIColor(
               red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
               green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
               blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
               alpha: CGFloat(1.0)
           )
       }
  
        
    private let servicesMenuCellIcons   = [
        settingsMenu(imageName: "services", entextName: "Services", artextName: "خدمات"),
        settingsMenu(imageName: "consulting", entextName: "consultations", artextName: "استشارات"),
        settingsMenu(imageName: "complainsts", entextName: "Complaints" , artextName: "الشكاوي")
    ]
    
    private let AppInfoCellIcons   = [
        
        PagerItem(imageName: "1", entextName: "Direct purchase from the application", artextName: "شراء مباشر من التطبيق", arDescription: "يوفر تطبيق اللبنة الذهبية العديد من وسائل الشراء والدفع لتسهيل عميلة البيع من خلال التطبيق والعميل بكل يسر .", enDescription: "The Golden Block application provides many means of purchase and payment to facilitate the sales process through the application and the customer with ease."),
        PagerItem(imageName: "2", entextName: "Secure account verification", artextName: "توثيق آمن للحساب", arDescription: "من مميزات تطبيق اللبنة الذهبية الامان والحماية من خلال فريق الدعم لدية لحماية جميع معلوماتك وبياناتك الشخصية .", enDescription: "One of the advantages of the Golden Brick application is the security and protection through its support team to protect all your information and personal data."),
        PagerItem(imageName: "3", entextName: "Ease of use", artextName: "سهولة الإستخدام", arDescription: "يمكنكم التعامل بكل سهولة باستخدام تطبيق اللبنة الذهبية دون ايجاد اى صعوبات فى الاستخدام .", enDescription: "You can deal with ease using the Gold brick application without finding any difficulties in using it.")
           
       ]
       

    
    func getServicesItems() -> [settingsMenu] {
        return servicesMenuCellIcons
    }
    
    func getPagerItems() -> [PagerItem] {
           return AppInfoCellIcons
       }
   
}


class settingsMenu {
    
    
    private(set) public var artextName :String
    private(set) public var entextName :String
    private(set) public var imageName :String
  
    
    init(imageName:String,entextName:String,artextName:String) {
        self.imageName = imageName
        self.artextName = artextName
        self.entextName = entextName
     
    }
}
    
    
    class PagerItem {
        
        
        private(set) public var artextName :String
        private(set) public var entextName :String
        private(set) public var arDescription :String
        private(set) public var enDescription :String
        private(set) public var imageName :String
        
        
        init(imageName:String,entextName:String,artextName:String,arDescription:String,enDescription:String) {
            self.imageName = imageName
            self.artextName = artextName
            self.entextName = entextName
            self.arDescription = arDescription
            self.enDescription = enDescription
            
        }
        
        
}



