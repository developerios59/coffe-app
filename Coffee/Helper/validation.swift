//
//  validation.swift
//  alphaCareProvider
//
//  Created by Mustafa Abdein on 11/29/19.
//  Copyright © 2019 Mustafa Abdein. All rights reserved.
//

import Foundation
import UIKit


class Validation: NSObject {
    
    public static let shared = Validation()
    
    func validate(values: (type: ValidationType, inputValue: String )...) -> Valid {
        for valueToBeChecked in values {
            switch valueToBeChecked.type {
            case .email:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .email, .emptyEmail, .inValidEmail)) {
                    return tempValue
                }
            case .phoneNo:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .phoneNo, .emptyPhone, .inValidPhone)) {
                    return tempValue
                }
            case .password:
                if let tempValue = isValidString((valueToBeChecked.inputValue, .password, .emptyPSW, .inValidPSW)) {
                    return tempValue
                }
            case .emptyName:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyName)) {
                    return tempValue
                }
                
            case .emptyphone:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyPhone)) {
                    return tempValue
                }
            case .emptyCity:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyCity)) {
                    return tempValue
                }
            case .emptyDistrict:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyDistrict)) {
                    return tempValue
                }
            case .emptyStreet:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyStreet)) {
                    return tempValue
                }
            case .emptyAddress:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyAddress)) {
                    return tempValue
                }
            case .emptyprice:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyprice)) {
                    return tempValue
                }
            case .emptyCode:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyCode)) {
                    return tempValue
                }
            case .passwordsMatched:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .passwordsMatched)) {
                    return tempValue
                }
                
            case .emptymessage:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptymessage)) {
                    return tempValue
                }
            case .emptycardNumber:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptycardNumber)) {
                    return tempValue
                }
                
            case .emptyBankName:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyBankName)) {
                    return tempValue
                }
                
                
            case .emptychargeValue:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptychargeValue)) {
                    return tempValue
                }
                
            case .emptyVCCCode:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyVCCCode)) {
                    return tempValue
                }
            case .emptyDate:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyDate)) {
                    return tempValue
                }
            case .emptyAccountName:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyAccountName)) {
                    return tempValue
                }
            case .balance:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .balance)) {
                    return tempValue
                }
            case .emptySubject:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptySubject)) {
                    return tempValue
                }
            case .emptyProjectDetails:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyProjectDetails)) {
                    return tempValue
                }
            case .emptyServiceDetails:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyServiceDetails)) {
                    return tempValue
                }
            case .emptyDetails:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyDetails)) {
                    return tempValue
                }
            case .emptyprivacyPolicy:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyprivacyPolicy)) {
                    return tempValue
                }
            case .emptyTime:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyTime)) {
                    return tempValue
                }
            case .emptycomment:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptycomment)) {
                    return tempValue
                }
            case .emptyNotes:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyNotes)) {
                    return tempValue
                }
            case .emptyCategory:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyCategory)) {
                    return tempValue
                }
            case .emptyCafeImage:
                if let tempValue = isValidEmpty((valueToBeChecked.inputValue, .emptyCafeImage)) {
                    return tempValue
                }
            case .confirmPassword:
                 if let tempValue = isValidString((valueToBeChecked.inputValue, .password, .emptyPSWConfirm, .inValidPSW)) {
                                   return tempValue
                               }
            }
        }
        return .success
        
    }
    
    func isValidString(_ input: (text: String, regex: RegEx , emptyAlert: AlertMessages, invalidAlert: AlertMessages)) -> Valid? {
        if input.text.isEmpty {
            return .failure(.error, input.emptyAlert.rawValue.localized)
        } else if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert.rawValue.localized)
        }
        return nil
    }
    
    func isValidRegEx(_ testStr: String, _ regex: RegEx) -> Bool {
        let stringTest = NSPredicate(format:"SELF MATCHES %@", regex.rawValue)
        let result = stringTest.evaluate(with: testStr)
        return result
    }
    
    func isValidEmpty (_ input:(testStr: String, emptyAlert: AlertMessages) ) -> Valid? {
        if input.testStr.isEmpty {
            // print("EMPTY\(emptyAlert.rawValue)")
            return .failure(.error, input.emptyAlert.rawValue.localized)
        }
        return nil
    }
    
    
    func isValidStringAndEmpty(_ input: (text: String, regex: RegEx , invalidAlert: AlertMessages)) -> Valid? {
        if isValidRegEx(input.text, input.regex) != true {
            return .failure(.error, input.invalidAlert.rawValue.localized)
        }
        return nil
    }
    
}


enum Alert {        //for failure and success results
    case success
    case failure
    case error
}

//for success or failure of validation with alert message
enum Valid {
    case success
    case failure(Alert, String)
}

enum ValidationType {
    case email
    case emptyName
    case phoneNo
    case password
    case confirmPassword
    case emptyAddress
    
    
    case emptyprice
    case emptyCode
    case emptyphone
    
    case emptyCity
    case emptyDistrict
    case emptyStreet
    
    case passwordsMatched
    case emptymessage
    
    case emptycardNumber
    case emptyBankName
    
    case emptyAccountName
    case emptychargeValue
    case emptyVCCCode
    
    case emptyDate
    case emptyTime
    case emptycomment
    
    case emptyNotes
    case emptyCategory
    case emptyCafeImage
    
    case balance
    case emptySubject
    case emptyProjectDetails
    case emptyServiceDetails
    case emptyDetails
    case emptyprivacyPolicy
    
}

enum RegEx: String {
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}" // Email
    case password = "^.{6,50}$" // Password length 6-15
    case alphabeticStringWithSpace = "^[a-zA-Z ]*$" // e.g. hello sandeep
    case alphabeticStringFirstLetterCaps = "^[A-Z]+[a-zA-Z]*$" // SandsHell
    case phoneNo = "[0-9]{9,15}" // PhoneNo 10-15 Digits
    case fileType = "^.*\\.(PDF)$"
    //    case "^[a-z][a-z, ]*$"
    
    //Change RegEx according to your Requirement
}


enum AlertMessages: String {
    case inValidEmail = "Email isn't in correct format"
    case inValidPhone = "Phone Number must not be less than 9 digits"
    case inValidPSW = "Password must be less than 6 digits"
    case emptyPhone = "Phone field is empty"
    case emptyEmail = "Email field is empty"
    case emptyPSW = "Password field is empty"
    case emptyPSWConfirm = "Confirm Password field is empty"
    case emptyName = "Name field is empty"
    
    case emptyCity = "Please enter the city name"
    case emptyDistrict = "Please enter the district name"
    case emptyStreet = "Please enter the street name"
    
    case emptyAddress = "please enter address"
    case emptyprice = "please enter price"
    case emptyCode = "please enter code"
    case passwordsMatched = "password fields doesn't match"
    case emptymessage = "message field is empty"
    
    case emptyAccountName = "Account Name field is empty"
    case emptychargeValue = "charge Value field is empty"
    case emptycardNumber  = "card Number field is empty"
    case emptyVCCCode = "CVV Code field is empty"
    case emptyBankName = "Bank Name field is empty"
    case emptyDate = "Date field is empty"
    case emptyTime = "Time field is empty"
    case emptycomment = "comment field is empty"
    
    case emptyNotes = "notes field is empty"
    case emptyCategory = "category field is empty"
    case emptyCafeImage = "please choose image for your cafe"
    
    case balance = "Your balance is less than the requested price"
    
    case emptySubject = "Subject field is empty"
    case emptyProjectDetails = "project Details field is empty"
    case emptyServiceDetails = "Service Details field is empty"
    case emptyDetails = "Details field is empty"
    
    case emptyTransferImg = "Please enter the transfer Image"
    case emptyprivacyPolicy = "please , approve on privacy policy"
    
}
