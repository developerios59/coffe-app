//
//  brandsVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class brandsVC: UIViewController , NVActivityIndicatorViewable{
    
    @IBOutlet weak var searchTF: UITextFieldX!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var brandsCollectionView: ContentSizedCollectionView!
    
    
    var brands = [Brand]()
    var searchedbrands = [Brand]()
    var searching = false

    override func viewDidLoad() {
        super.viewDidLoad()

        brandsCollectionView.delegate = self
        brandsCollectionView.dataSource = self
       
        searchView.layer.cornerRadius = searchView.frame.height / 2
         searchView.layer.borderWidth = 1
        searchView.layer.borderColor = Helper.standard.hexStringToUIColor(hex: "BFBFBF").cgColor
        
        searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: .editingChanged)
        
        getBrands()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !textField.text!.isEmpty {
            searchedbrands = brands.filter({$0.name.contains(textField.text!)})
            searching = true
            self.brandsCollectionView.isHidden = searchedbrands.isEmpty
            self.brandsCollectionView.reloadData()
        }else {
            searching = false
            self.brandsCollectionView.isHidden = false
            self.brandsCollectionView.reloadData()
        }
    }
       
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
    }
    
    func getBrands() {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.brands(lang: "en".localized).send(brandModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            
                        }else{
                            self.brands = model.data
                            self.brandsCollectionView.reloadData()
                        }
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    

   

}


extension brandsVC : UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searching ? searchedbrands.count : brands.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "brandCell", for: indexPath) as! brandCell
        cell.configureCell(data: self.searching ? self.searchedbrands[indexPath.row] :  self.brands[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let categoryScene =  categoryVC.instantiate(fromAppStoryboard: .Category)
        categoryScene.categoryName = self.searching ? self.searchedbrands[indexPath.row].name : self.brands[indexPath.row].name
        categoryScene.categoryId =  self.searching ? String(self.searchedbrands[indexPath.row].id) : String(self.brands[indexPath.row].id)
        categoryScene.categoryImage =  self.searching ? self.searchedbrands[indexPath.row].image : self.brands[indexPath.row].image
        categoryScene.type = "brand"
        self.navigationController?.pushViewController(categoryScene, animated: true)
    }
    
    
}
