//
//  brandModel.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - brandModel
struct brandModel: Codable,CodableInit {
    let value, key: String
    let data: [Brand]
}

