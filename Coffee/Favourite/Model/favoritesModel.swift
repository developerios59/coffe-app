//
//  favoritesModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 9/3/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - favoritesModel
struct favoritesModel: Codable ,CodableInit{
    let value, key: String
    let data: [favoritesData]
}

// MARK: - favoritesData
struct favoritesData: Codable ,CodableInit{
    let id, productID: Int
    let image: String
    let name: String
    let rate: Int
    let price: String

    enum CodingKeys: String, CodingKey {
        case id
        case productID = "product_id"
        case image, name, rate, price
    }
}
