//
//  favoriteVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class favoriteVC: UIViewController ,NVActivityIndicatorViewable {

    @IBOutlet weak var collectionView: ContentSizedCollectionView!
    @IBOutlet weak var emptyLB :UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    let userId = Helper.standard.userId()
    
    var favorites = [favoritesData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.tabBar.isTranslucent = true
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        
           backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
            self.navigationController?.popViewController(animated: true)
            
        }
          
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
         self.navigationController?.navigationBar.isHidden = false
        
        getFavorites()
        
    }
    
    
    func getFavorites() {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.favorites(lang:  "en".localized).send(favoritesModel.self)  { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            self.collectionView.isHidden = true
                            self.emptyLB.isHidden = false
                        }else{
                            self.collectionView.isHidden = false
                            self.emptyLB.isHidden = true
                            self.favorites = model.data
                            self.collectionView.reloadData()
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    

}


extension favoriteVC : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favorites.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favoriteCell", for: indexPath) as! favoriteCell
        cell.configureCell(data: self.favorites[indexPath.row])
        
        cell.favoriteAction = { [unowned self] in
            if Reachability.isConnectedToNetwork(){
                self.startAnimating()
                UserRouter.favorite(lang: "en".localized, product_id: String(self.favorites[indexPath.row].productID)).send(favoriteAddModel.self)  { (response) in
                    self.stopAnimating()
                    switch response {
                    case .failure(let error):
                        // TODO: - Handle error as you want, printing isn't handling.
                        print(error)
                    case .success(let model):
                        print(model)
                        if model.value == "0" {
                            
                        }else if model.value == "1" {
                            if model.data == 0 {
                            self.favorites.remove(at: indexPath.row)
                            self.emptyLB.isHidden = !self.favorites.isEmpty
                            self.collectionView.isHidden = self.favorites.isEmpty
                                
                                self.collectionView.reloadData()
                            }
                        }
                    }
                }
            }else{
                Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
            }
            
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
              let width = collectionView.frame.width - 20
              return CGSize(width: width / 2, height: 250)
          }
    
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productScene =  productVC.instantiate(fromAppStoryboard: .Products)
            productScene.productImage = self.favorites[indexPath.row].image
            productScene.productId =  String(self.favorites[indexPath.row].productID)
        self.navigationController?.pushViewController(productScene, animated: true)
    }
      
   
}
