//
//  favoriteCell.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import Cosmos

class favoriteCell: UICollectionViewCell {
    
    @IBOutlet weak var favouriteBtn: UIButton!
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var nameLB: UILabel!
    @IBOutlet weak var priceLB: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    
    var favoriteAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        favouriteBtn.layer.cornerRadius = favouriteBtn.frame.height / 2 
    }
    
    
    
    func configureCell(data:favoritesData) {
        self.favouriteBtn.setImage(UIImage(named:"heart"), for: .normal)
        self.img.kf.setImage(with: URL(string: data.image))
        self.nameLB.text = data.name
        self.priceLB.text = data.price
      self.ratingView.rating = Double(data.rate)
    }
    
    @IBAction func didSelectFavoriteBu(_ sender: Any) {
        favoriteAction?()
    }
}
