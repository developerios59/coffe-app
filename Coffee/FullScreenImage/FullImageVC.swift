//
//  FullImageVC.swift
//  birlasoftONE
//
//  Created by Dharmendra Chaudhary on 26/02/20.
//  Copyright © 2020 Birlasoft Ltd. All rights reserved.
//

import UIKit
//import SDWebImage

class FullImageVC: UIViewController , UIScrollViewDelegate {

    @IBOutlet weak var imageView: UIImageView!
    var imageUrl:String?
    var imageToShow = UIImage(named: "language_background")
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let url = imageUrl {
            self.imageView.kf.setImage(with: URL(string: url))
        }else{
            imageView.image = imageToShow
        }
    }

    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
