//
//  cartVC.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView
import RealmSwift

class cartVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var tableView: IntrinsicTableView!
//  @IBOutlet weak var deliveryLB: UILabel!
    @IBOutlet weak var taxLB: UILabel!
    @IBOutlet weak var totalLB: UILabel!
    @IBOutlet weak var similarCollectionView: UICollectionView!
    @IBOutlet weak var priceStackView: UIStackView!
    @IBOutlet weak var similarProductsLB: UILabel!
    @IBOutlet weak var nextBtn: UIButtonX!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var emptyLB: UILabel!
    
    let userDefault = UserDefaults.standard
    let userId = Helper.standard.userId()
    var cartProducts : Results<CartItem>?
    var similarProducts = [similarProductsData]()
    var requestedOrders =  Set<cartModel>()
    
    var tax = ""
    var delivery = 0
    var total = 0
    var price = 0.0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left"), for: .normal)
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        similarCollectionView.delegate = self
        similarCollectionView.dataSource = self
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        getCartProducts()
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getPrices(totalBeforeTax:Int) {
        if Reachability.isConnectedToNetwork() {
            self.startAnimating()
            UserRouter.costs.send(costModel.self) { [self] (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else  if model.value == "1" {
                
                        if  let tax = Int(model.data.tax) {
                            self.tax = model.data.tax

                            let taxPrice = (self.price) * Double(tax) / 100
                            self.taxLB.text = "\(taxPrice)"  + " SAR".localized
                            let totalWithoutDelivery = Double(totalBeforeTax) + Double(taxPrice)
                            
                            let total = totalWithoutDelivery  + 25
                            
                            self.totalLB.text = "\(total)" + " SAR".localized
                            

                            UserDefaults.standard.setValue(taxPrice, forKey: Constants.CoffeeConstant.taxPrice.rawValue)
                            UserDefaults.standard.setValue(total, forKey: Constants.CoffeeConstant.totlaPrice.rawValue)
                            self.total = Int(totalWithoutDelivery)
                            userDefault.setValue(total, forKey: "totalToOrder")
                            userDefault.setValue(taxPrice, forKey: "taxToOrder")
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    func getSimilarProducts(productId:String) {
        if Reachability.isConnectedToNetwork() {
            self.startAnimating()
            UserRouter.similarProducts(lang: "en".localized, product_id: productId).send(similarProductsModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                    }else  if model.value == "1" {
                        if model.data.isEmpty {
                            self.similarCollectionView.isHidden = true
                            self.similarProductsLB.isHidden = true
                        }else{
                            
                            self.similarProducts = model.data
                            self.similarCollectionView.reloadData()
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
    
    
    func getCartProducts() {
        do {
            let items = try Realm().objects(CartItem.self)
            let filteredItems = items.filter("userId = '\(userId)' OR userId = ''")
            
            
            cartProducts = filteredItems
            if filteredItems.isEmpty {
                self.scrollView.isHidden = true
                self.emptyLB.isHidden = false
            }else{
                self.scrollView.isHidden = false
                self.emptyLB.isHidden = true
                let Total = self.cartProducts!.reduce(0) {$0 + ( Int($1.orderPrice)  * Int($1.orderQuantity)) }
                self.price  = Double(Total)
                getPrices(totalBeforeTax: Total)
                getSimilarProducts(productId: filteredItems[0].productID)
            }
            self.tableView.reloadData()
            
            for item in items {
                self.requestedOrders.insert(cartModel(productID: item.productID, count: String(item.count), price: String(item.orderPrice)))
            }
        }catch {
            print(error)
        }
    }
    
    @IBAction func didSelectNextBu(_ sender: Any) {
        if self.userId == "" {
            Helper.standard.showVisitorPopUp(vc: self)
        }else{
            CoffeeClient.getTokenAccess() { (response, error) in
                if response?.isSuccess == true {
                    let myToken = response?.resultData?.accessToken ?? ""
                    print("the token is = \(myToken) ")
                    UserDefaults.standard.set(myToken, forKey: Constants.CoffeeConstant.accessToken.rawValue)
                }else{
                    print("the error data is = \(response?.messageEn ?? "" )")
                    return
                }
            }
            
            let deliveryInfoScene =  deliveryInfoVC.instantiate(fromAppStoryboard: .DeliveryInfo)
            deliveryInfoScene.cartOrders = Array(self.requestedOrders)
            deliveryInfoScene.tax = self.tax
            deliveryInfoScene.delivery = self.delivery
            deliveryInfoScene.total = self.total
            self.navigationController?.pushViewController(deliveryInfoScene, animated: true)
        }
    }
}


extension cartVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let cart = self.cartProducts else{return 0}
        
        return cart.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cartCell", for: indexPath) as! cartCell

        guard let cart =  self.cartProducts else{return cell }
        cell.configureCell(data: cart[indexPath.row])
        cell.deleteAction = { [unowned self] in
            let alert = UIAlertController(title: "Item Delete".localized, message: "Are you Sure?".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Yes".localized, style: .default, handler: { (_) in
                do {
                    let realm = try Realm()
                    try realm.write {
                        if  let deleteData = realm.object(ofType: CartItem.self,forPrimaryKey: cart[indexPath.row].id) {
                            realm.delete(deleteData)
                        }else{
                            print("Not exist")
                        }
                        self.getCartProducts()
                    }
                }catch{
                    print(error)
                }
            }))
            alert.addAction(UIAlertAction(title: "No".localized, style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        return cell
    }
}


extension cartVC : UICollectionViewDataSource , UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return similarProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "similarProductsCell", for: indexPath) as! similarProductsCell
        cell.configureCell(data: self.similarProducts[indexPath.row])
        return  cell
    }
}
