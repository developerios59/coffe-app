//
//  similarProductsModel.swift
//  Coffee
//
//  Created by Mustafa on 10/15/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - similarProductsModel
struct similarProductsModel: Codable ,CodableInit{
    let value, key: String
    let data: [similarProductsData]
}

// MARK: - similarProductsData
struct similarProductsData: Codable,CodableInit {
    let id: Int
    let image: String
    let name, datumDescription: String
    let rate: Int
    let price: String

    enum CodingKeys: String, CodingKey {
        case id, image, name
        case datumDescription = "description"
        case rate, price
    }
}

