//
//  cartItem.swift
//  goldenBrickAgent
//
//  Created by saleh on 9/3/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation
import RealmSwift

class CartItem : Object {
    @objc dynamic var id = 0
    @objc dynamic var orderQuantity = 0
    @objc dynamic var orderName = ""
    @objc dynamic var orderdescription = ""
    @objc dynamic var orderPrice = 0.0
    @objc dynamic var orderImage = ""
    @objc dynamic var productID = ""
    @objc dynamic var count = 0.0
    @objc dynamic var userId = ""
//    @objc dynamic var totlalPriceForQuantity = ""
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
