//
//  cartModel.swift
//  Coffee
//
//  Created by Mustafa on 10/14/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct cartModel: Codable, Hashable, Equatable {
    let productID: String
       let count: String
       let price: String

       enum CodingKeys: String, CodingKey {
           case productID = "product_id"
           case count, price
       }
    
    
    static func ==(lhs: cartModel, rhs: cartModel) -> Bool {
           return lhs.productID == rhs.productID && lhs.count == rhs.count &&  lhs.price == rhs.price
       }
    
}
