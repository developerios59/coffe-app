//
//  costModel.swift
//  Coffee
//
//  Created by Mustafa on 10/14/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct costModel: Codable ,CodableInit{
    let value, key: String
    let data: costData
}

// MARK: - costData
struct costData: Codable ,CodableInit{
    let deliveryPrice, tax: String

    enum CodingKeys: String, CodingKey {
        case deliveryPrice = "delivery_price"
        case tax
    }
}
