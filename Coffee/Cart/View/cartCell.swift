//
//  cartCell.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class cartCell: UITableViewCell {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLB: UILabel!
    @IBOutlet weak var productDetailsLB: UILabel!
    @IBOutlet weak var productPriceLB: UILabel!
    
     var deleteAction : (()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(data:CartItem) {
        productImg.kf.setImage(with: URL(string: data.orderImage))
        productNameLB.text = data.orderName
        productDetailsLB.text = data.orderdescription
        productPriceLB.text = String(Int(data.orderPrice) * Int(data.count)) + " SAR".localized
    }
    
     @IBAction func didSelectdeleteBu(_ sender: Any) {
            deleteAction?()
        }
}
