//
//  successProcessVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class successProcessVC: UIViewController {
    
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var subtitleLB: UILabel!
    @IBOutlet weak var detailLB: UILabel!
    
      let mainStoryboard = AppStoryboard.Main.instance
    
    var isService = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    
    @IBAction func didSelectMainBu(_ sender: Any) {
        let mainScene = self.mainStoryboard.instantiateViewController(withIdentifier: "mainTabBar")
        UIApplication.shared.windows.first?.rootViewController = mainScene
        UIApplication.shared.windows.first?.makeKeyAndVisible()
    }
    
}
