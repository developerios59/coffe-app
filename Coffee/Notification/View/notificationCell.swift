//
//  notificationCell.swift
//  cleanAppClient
//
//  Created by saleh on 5/18/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class notificationCell: UITableViewCell {

   
    @IBOutlet weak var notifyLB: UILabel!
    @IBOutlet weak var timeLB: UILabel!
    
   
    
    override func awakeFromNib() {
        super.awakeFromNib()
       
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

  
    
    func configureCell(data:notificationData) {
        notifyLB.text = data.content
          timeLB.text = data.content
    }
    
}
