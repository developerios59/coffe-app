//
//  notificationVC.swift
//  cleanAppClient
//
//  Created by saleh on 5/17/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class notificationVC: UIViewController ,NVActivityIndicatorViewable{
    
 
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLB: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    let userId = Helper.standard.userId()
    
    
    var notifications = [notificationData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
          setupUI()
        if self.userId == "" {
            Helper.standard.showVisitorPopUp(vc: self)
        }else{
         getNotifications()
        }
    }
    
    func setupUI() {
        tableView.delegate = self
        tableView.dataSource = self
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
       backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
 
    
    func getNotifications() {
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.notifications(lang: "en".localized).send(notificationModel.self)  { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            self.emptyLB.isHidden = false
                            self.tableView.isHidden = true
                            
                        }else{
                            self.emptyLB.isHidden = true
                            self.tableView.isHidden = false
                            
                            self.notifications = model.data
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
}

extension notificationVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! notificationCell
        cell.configureCell(data: notifications[indexPath.row])
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
           if editingStyle == .delete {
               
               let notificationId = String(notifications[indexPath.row].id)
               var notifyDeleteResponse: HandleResponse<notificationDeleteModel> {
                   return {[weak self] (response) in
                       self?.stopAnimating()
                       switch response {
                       case .failure(let error):
                           // TODO: - Handle error as you want, printing isn't handling.
                           print(error)
                       case .success(let model):
                           print(model)
                           if model.value == "0" {
                               Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                           }else if model.value == "1" {
                               self?.notifications.remove(at: indexPath.row)
                               tableView.deleteRows(at: [indexPath], with: .right)
                               if self!.notifications.isEmpty {
                                   self?.tableView.isHidden = true
                               }
                               self?.tableView.reloadData()
                           }
                       }
                   }
               }
               // Api request
               if Reachability.isConnectedToNetwork(){
                               self.startAnimating()

                UserRouter.deleteNotification(lang: "en".localized, notification_id: notificationId).send(notificationDeleteModel.self, then: notifyDeleteResponse)
               }else{
                   Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
               }
               
           }
       }
    
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
}
