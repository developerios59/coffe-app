//
//  notificationDeleteModel.swift
//  cleanAppClient
//
//  Created by saleh on 7/20/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - notificationDeleteModel
struct notificationDeleteModel: Codable ,CodableInit{
    let value, key: String
    
    let msg,data: String?
}
