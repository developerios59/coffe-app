//
//  notificationModel.swift
//  cleanAppClient
//
//  Created by saleh on 7/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - notificationModel
struct notificationModel: Codable ,CodableInit{
    let value, key: String
    let data: [notificationData]
}

// MARK: - notificationData
struct notificationData: Codable ,CodableInit{
    let id: Int
    let content: String
    let orderID: Int
    let created: String

    enum CodingKeys: String, CodingKey {
        case id, content
        case orderID = "order_id"
        case created
    }
}


// MARK: - notificationsCount
struct notificationsCount: Codable , CodableInit {
    let value, key: String
    let data: Int
}
