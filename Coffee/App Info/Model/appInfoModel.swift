//
//  appInfoModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - appInfoModel
struct appInfoModel: Codable,CodableInit {
    let value, key, data: String
}
