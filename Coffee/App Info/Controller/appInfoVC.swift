//
//  appInfoVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class appInfoVC: UIViewController ,NVActivityIndicatorViewable{
    
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var titleLB: UILabel!
    @IBOutlet weak var headrImg: UIImageView!
    @IBOutlet weak var backBtn: UIButton!
    
    
// MARK :- about App --> 0   ---  privacy policy --> 1
    var typeIndex = 0
    var headerName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getAppInfo(index: typeIndex, lang: "en".localized)
         backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
        customNavBar()
      
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
    }
    
    func customNavBar() {
       
        switch typeIndex {
        case 0:
            titleLB.text = "About App".localized
            headrImg.image = UIImage(named: "vicort")
        case 1:
             titleLB.text = "Privacy policy".localized
        headrImg.image = UIImage(named: "vico")
             
        default:
            break
        }
        
    }
          
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getAppInfo(index:Int , lang : String) {
        
        var appInfoResponse: HandleResponse<appInfoModel> {
                  return {[weak self] (response) in
                      self?.stopAnimating()
                      switch response {
                      case .failure(let error):
                          // TODO: - Handle error as you want, printing isn't handling.
                          print(error)
                      case .success(let model):
                          print(model)
                          self?.descriptionTextView.text = model.data
                          
                    }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            switch index {
            case 0:
                UserRouter.about(lang: lang).send(appInfoModel.self, then: appInfoResponse)
            case 1:
                 UserRouter.terms(lang: lang).send(appInfoModel.self, then: appInfoResponse)
            default:
                break
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
        
    }
    


}
