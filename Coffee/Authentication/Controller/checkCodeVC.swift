//
//  checkCodeVC.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class checkCodeVC: UIViewController ,UITextFieldDelegate , NVActivityIndicatorViewable  {
    
   
    @IBOutlet weak var codeLB: UILabel!
    @IBOutlet weak var codeTF: UITextFieldX!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var codeStackView: UIStackView!
    
    
    var isActivationCode = false
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeLB.text = isActivationCode ? "Activation code".localized : "Validation code".localized
       
        codeStackView.semanticContentAttribute = .forceLeftToRight
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didSelectSendBu(_ sender: Any) {
        // Validation
        let response = Validation.shared.validate(values: (ValidationType.emptyCode,codeTF.text!))
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        
        // Api Request
        var codeActivationResponse: HandleResponse<checkCodeModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    //  print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                    }else if model.value == "1" {
                        if self!.isActivationCode {
                            self?.navigationController?.popToRootViewController(animated: true)

                        }else{
                            UserDefaults.standard.set( "Bearer " + model.data!.token, forKey: "ToKen")
                            let changePasswordScene = changePasswordVC.instantiate(fromAppStoryboard: .authentication)
                            changePasswordScene.code = model.data!.code
                            self?.navigationController?.pushViewController(changePasswordScene, animated: true)
                            
                        }
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork() {
            self.startAnimating()
            UserRouter.checkCode(code: self.codeTF.text!, lang: "en".localized).send(checkCodeModel.self, then: codeActivationResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
    @IBAction func didSelectResendCodeBu(_ sender: Any) {
        // Api Request
        var resendCodeResponse: HandleResponse<resendCodeModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg, vc: self!)
                    }else if model.value == "1" {
                        Helper.standard.displayAlert(title: "Success".localized, message: model.msg, vc: self!)
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.resendCode( lang: "en".localized).send(resendCodeModel.self, then: resendCodeResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
}


extension checkCodeVC : OTPDelegate {
    
    func didChangeValidity(isValid: Bool) {

        //  Helper.standard.displayAlert(title: "Error", message: "", vc: self)
    }
    
}
