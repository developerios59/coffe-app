//
//  languageChooseVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/26/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH

class languageChooseVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @IBAction func didSelectArabicBu(_ sender: Any) {
        MOLH.setLanguageTo("ar")
        MOLH.reset()
    }
    
    @IBAction func didSelectEnglishBu(_ sender: Any) {
        MOLH.setLanguageTo("en")
        MOLH.reset()

    }
    
}
