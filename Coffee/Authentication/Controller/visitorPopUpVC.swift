//
//  visitorPopUpVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class visitorPopUpVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func didSelectSignInBu(_ sender: Any) {
        let signInScene = signInVC.instantiate(fromAppStoryboard: .authentication)
        let navigation = UINavigationController(rootViewController: signInScene)
        navigation.modalPresentationStyle = .fullScreen
        navigation.navigationBar.barStyle = .black
        self.present(navigation, animated: true, completion: nil)
    }
    

    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
