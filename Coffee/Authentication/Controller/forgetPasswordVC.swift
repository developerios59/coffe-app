//
//  forgetPasswordVC.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class forgetPasswordVC: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var mobileNumTF: UITextFieldX!
     @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var forgetPasswodStackView: UIStackView!
    
    
     let deviceId = PushNotificationManager().PushTokenIfNeeded()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        forgetPasswodStackView.semanticContentAttribute = .forceLeftToRight
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
          sendNum()
           return true
       }
    

    
    func sendNum() {
        // Validation
        let response = Validation.shared.validate(values: (ValidationType.phoneNo, mobileNumTF.text!))
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        // Api Request
        var forgetPassResponse: HandleResponse<signModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                    }else if model.value == "1" {
                        UserDefaults.standard.set("Bearer " + model.data!.token, forKey: "ToKen")
                        
                        let checkCodeScene = checkCodeVC.instantiate(fromAppStoryboard: .authentication)
                        checkCodeScene.isActivationCode = false
                        self?.navigationController?.pushViewController(checkCodeScene, animated: true)
                        
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.forgetPass(phone: mobileNumTF.text!, lang: "en".localized).send(signModel.self, then: forgetPassResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }

    
    @IBAction func didSelectNextBu(_ sender: Any) {
           sendNum()
       }
   
}
