//
//  signUpVC.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView


protocol LocationSelectedProtocol {
    func popupValueSelected(enAddress: String,arAddress: String, lat: String, lng: String)
}

class signUpVC: UIViewController ,UITextFieldDelegate ,NVActivityIndicatorViewable {

    @IBOutlet weak var mobileNumTF: UITextFieldX!
    @IBOutlet weak var nameTF: UITextFieldX!
    @IBOutlet weak var emailTF: UITextFieldX!
    @IBOutlet weak var locationTF: UITextFieldX!
    @IBOutlet weak var passwordTF: UITextFieldX!
    @IBOutlet weak var confirmPasswordTF: UITextFieldX!
    @IBOutlet weak var backBtn: UIButton!


    @IBOutlet weak var nameStackView: UIStackView!
    @IBOutlet weak var mobileNumberStackView: UIStackView!
    @IBOutlet weak var emailStackView: UIStackView!
    @IBOutlet weak var addressStackView: UIStackView!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var passwordConfirmStackView: UIStackView!
    
    
     let deviceId = PushNotificationManager().PushTokenIfNeeded()
    
    var latitude = ""
    var longtitude = ""
    var address = ""
    var passwordsMatch = ""
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    
    func setupUI() {
        mobileNumTF.delegate = self
        nameTF.delegate = self
        locationTF.delegate = self
        emailTF.delegate = self
        passwordTF.delegate = self
        confirmPasswordTF.delegate = self
        
        nameStackView.semanticContentAttribute = .forceLeftToRight
        mobileNumberStackView.semanticContentAttribute = .forceLeftToRight
        emailStackView.semanticContentAttribute = .forceLeftToRight
        addressStackView.semanticContentAttribute = .forceLeftToRight
        passwordStackView.semanticContentAttribute = .forceLeftToRight
        passwordConfirmStackView.semanticContentAttribute = .forceLeftToRight
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
       
    
   
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
         if  textField == locationTF {
             view.endEditing(true)
            
            let mapScene = mapVC.instantiate(fromAppStoryboard: .authentication)
           
             mapScene.delegate = self
             self.navigationController?.pushViewController(mapScene, animated: true)
            return false
         }
        return true
     }

    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if  textField == confirmPasswordTF || textField == passwordTF {
            if confirmPasswordTF.text == passwordTF.text {
                passwordsMatch = "matched"
            }else{
                passwordsMatch = ""
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        signUp()
        return true
    }
    
  

    
    func signUp() {
     self.view.endEditing(true)
        // Validation
        
        
        let response = Validation.shared.validate(values:(ValidationType.phoneNo , mobileNumTF.text!),(ValidationType.emptyName, nameTF.text!),(ValidationType.emptyAddress , locationTF.text!),(ValidationType.email , emailTF.text!),(ValidationType.password , passwordTF.text!),(ValidationType.confirmPassword , confirmPasswordTF.text!),(ValidationType.passwordsMatched , passwordsMatch))
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        
        // Api Request
        var signUpResponse: HandleResponse<signUpModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                    }else if model.value == "1" {
                         UserDefaults.standard.set( "Bearer " + model.data!.token, forKey: "ToKen")
                        let checkCodeScene = checkCodeVC.instantiate(fromAppStoryboard: .authentication)
                            checkCodeScene.isActivationCode = true
                        self?.navigationController?.pushViewController(checkCodeScene, animated: true)
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.SignUp(phone: mobileNumTF.text!, name: nameTF.text!, email: emailTF.text!, address: locationTF.text!, lat: latitude, lng: longtitude, password: passwordTF.text!, device_id: deviceId, device_type: "ios", lang: "en".localized).send(signUpModel.self, then: signUpResponse)
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    @IBAction func didSelectSignUpBu(_ sender: Any) {
          signUp()
      }

  
    @IBAction func didSelectSignInBu(_ sender: Any) {
        if self.navigationController?.viewControllers.previous is signInVC {
            self.navigationController?.popViewController(animated: true)
        } else {
            let signInScene = signInVC.instantiate(fromAppStoryboard: .authentication)
            self.navigationController?.pushViewController(signInScene, animated: true)
        }
    }

    
}


extension signUpVC : LocationSelectedProtocol {
    func popupValueSelected(enAddress: String, arAddress: String, lat: String, lng: String) {
    locationTF.text = arAddress
         self.address = arAddress
        self.latitude = lat
        self.longtitude = lng
    }
}


