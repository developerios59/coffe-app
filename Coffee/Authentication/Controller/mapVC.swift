//
//  mapVC.swift
//  alphaCareProvider
//
//  Created by Mustafa Abdein on 12/2/19.
//  Copyright © 2019 Mustafa Abdein. All rights reserved.
//

import UIKit
import GoogleMaps
import MOLH

class mapVC: UIViewController ,CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var saveBT: UIButtonX!
    @IBOutlet weak var backBtn: UIButtonX!
    
    
    var delegate: LocationSelectedProtocol?
    var locationManager = CLLocationManager()
    var marker = GMSMarker()
    
    var address: String!
    var lat: String!
    var lng: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "left" : "right" ), for: .normal)
        locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        self.mapView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
        marker.position = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        
        self.lat = "\(userLocation!.coordinate.latitude)"
        self.lng = "\(userLocation!.coordinate.longitude)"
        
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 15);
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
        marker.map = self.mapView
        
        marker.title = ""
        //marker.icon = #imageLiteral(resourceName: "noun_Location")
        marker.isDraggable = true
        locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            // manager.requestWhenInUseAuthorization()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            //  manager.requestWhenInUseAuthorization()
            break
        default:
            break
        }
    }
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
    func reverseGeocodeLocation(_ location: CLLocation) {
//        if MOLHLanguage.
        let locale = Locale(identifier: "ar")
        
        CLGeocoder().reverseGeocodeLocation(location, preferredLocale: locale) { placemarks, error in
            
            if let placemark = placemarks?.first {
                DispatchQueue.main.async {
                    //  update UI here
                    let address = (placemark.name ?? "") + " " + (placemark.subLocality ?? "" )
                        let fullAddress = address + " " + (placemark.locality ?? "")
                    
                    self.address = fullAddress
                    UIView.animate(withDuration: 0.25) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    

    
    
    @IBAction func didSelectSaveLocationBu(_ sender: Any) {
        if let address = self.address ,let  latitude = self.lat ,let langtiude = self.lng{
            delegate?.popupValueSelected(enAddress: address, arAddress: address, lat: latitude, lng: langtiude)
            if navigationController != nil {
                navigationController?.popViewController(animated: true)
            }else{
                self.dismiss(animated: true, completion: nil)
            }
        }else{
            let alert = UIAlertController(title: "", message: "Please Enable Your location".localized, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (_) in
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    // If general location settings are enabled then open location settings for the app
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}

extension mapVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        print("clicked on marker")
    }
    
    func mapView(_ mapView: GMSMapView, didLongPressInfoWindowOf marker: GMSMarker) {
        print("long press on window")
    }
    
   
    
    func mapView(_ mapView: GMSMapView, didBeginDragging marker: GMSMarker) {
        print("draging start")
    }
    
    func mapView(_ mapView: GMSMapView, didDrag marker: GMSMarker) {
        
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        marker.position = coordinate
        
        
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        
        reverseGeocodeLocation(location)
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let location = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        
        reverseGeocodeLocation(location)
        
    }
}



