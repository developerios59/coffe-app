//
//  signInVC.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class signInVC: UIViewController ,UITextFieldDelegate ,NVActivityIndicatorViewable {
    
    @IBOutlet weak var phoneNumTF: UITextFieldX!
    @IBOutlet weak var passwordTF: UITextFieldX!
    @IBOutlet weak var mobileStackView: UIStackView!
    @IBOutlet weak var passwordStackView: UIStackView!
    @IBOutlet weak var backBtn: UIButton!
    
    let mainStoryboard = AppStoryboard.Main.instance
    
    let deviceId = PushNotificationManager().PushTokenIfNeeded()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
        phoneNumTF.delegate = self
        passwordTF.delegate = self
        mobileStackView.semanticContentAttribute = .forceLeftToRight
        passwordStackView.semanticContentAttribute = .forceLeftToRight
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        signIn()
        return true
    }
    
    @IBAction func didSelectForgetPassBu(_ sender: Any) {
        let forgetPasswordScene = forgetPasswordVC.instantiate(fromAppStoryboard: .authentication)
                 self.navigationController?.pushViewController(forgetPasswordScene, animated: true)
    }
    
    func signIn(){
        self.view.endEditing(true)
        // Validation
        let response = Validation.shared.validate(values: (ValidationType.phoneNo, phoneNumTF.text!),(ValidationType.password , passwordTF.text!))
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        
        // Api Request
        var loginResponse: HandleResponse<signModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0"  {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                    }else if model.value == "1" {
                        if !model.data!.userType.contains("user")  {
                            Helper.standard.displayAlert(title: "Error".localized, message: "this user is \(model.data!.userType) type".localized, vc: self!)
                        }else{
                            
                             UserDefaults.standard.set( "Bearer " + model.data!.token, forKey: "ToKen")
                             UserDefaults.standard.set( model.data!.token, forKey: "UserId")
                            UserDefaults.standard.set(model.data?.avatar, forKey: "avatar")
                            UserDefaults.standard.set(model.data?.name, forKey: "userName")
                            UserDefaults.standard.set(model.data?.email, forKey: "email")
                            UserDefaults.standard.set(model.data?.phone, forKey: "userMobileNumber")
                            UserDefaults.standard.set(model.data?.deviceID, forKey: "deviceID")
                            
                            let mainScene = self?.mainStoryboard.instantiateViewController(withIdentifier: "mainTabBar")
                            
                            UIApplication.shared.windows.first?.rootViewController = mainScene
                            UIApplication.shared.windows.first?.makeKeyAndVisible()
                        }
                        
                    }else if model.value == "2" {
                         UserDefaults.standard.set( "Bearer " + model.data!.token, forKey: "ToKen")
                        let checkCodeScene = checkCodeVC.instantiate(fromAppStoryboard: .authentication)
                        checkCodeScene.isActivationCode = true
                        self?.navigationController?.pushViewController(checkCodeScene, animated: true)
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.signIn(phone: self.phoneNumTF.text!, password: self.passwordTF.text!, device_id: deviceId, device_type: "ios", lang: "en".localized).send(signModel.self, then: loginResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    @IBAction func didSelectSignInBu(_ sender: Any) {
        signIn()
    }
    
    
    @IBAction func didSelectSignUpBu(_ sender: Any) {
        if self.navigationController?.viewControllers.previous is signUpVC {
            self.navigationController?.popViewController(animated: true)
        } else {
           let signUpScene = signUpVC.instantiate(fromAppStoryboard: .authentication)
            self.navigationController?.pushViewController(signUpScene, animated: true)

        }
    }
    
}

