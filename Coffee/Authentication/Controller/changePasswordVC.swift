//
//  changePasswordVC.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class changePasswordVC: UIViewController ,UITextFieldDelegate ,NVActivityIndicatorViewable {

    @IBOutlet weak var passwordTF: UITextFieldX!
    @IBOutlet weak var passwordConfirmTF: UITextFieldX!
     @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var neewPasswordStackView: UIStackView!
    @IBOutlet weak var confirmPasswordStackView: UIStackView!

   
    var code = ""
    var passwordsMatch = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

       passwordTF.delegate = self
        passwordConfirmTF.delegate = self
        
        neewPasswordStackView.semanticContentAttribute = .forceLeftToRight
        confirmPasswordStackView.semanticContentAttribute = .forceLeftToRight
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
            
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
           self.navigationController?.popViewController(animated: true)
       }
       
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           updatePass()
           return true
       }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if  textField == passwordConfirmTF || textField == passwordTF {
            if passwordConfirmTF.text == passwordTF.text {
                passwordsMatch = "matched"
            }else{
                passwordsMatch = ""
            }
        }
    }
    
    @IBAction func didSelectBackBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  
    @IBAction func didSelectConfirmBu(_ sender: Any) {
        updatePass()
        
    }
    
    
    func updatePass() {
        // Validation
        let response = Validation.shared.validate(values: (ValidationType.password , passwordTF.text!),(ValidationType.confirmPassword , passwordConfirmTF.text!),(ValidationType.passwordsMatched , passwordsMatch))
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        
        // Api Request
        var updatePassResponse: HandleResponse<signModel> {
            return {[weak self] (response) in
                self?.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        Helper.standard.displayAlert(title: "Error".localized, message: model.msg!, vc: self!)
                    }else if model.value == "1" {
                        
                            let alert = UIAlertController(title: "Success".localized, message: "your password is changed".localized, preferredStyle: .alert)
                            
                            alert.addAction(UIAlertAction(title: "Ok".localized, style: .default, handler: { (_) in
                                    self?.navigationController?.popToRootViewController(animated: true)
                            }))
                            
                            self?.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
        
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.updatePass(code: self.code, password: passwordTF.text!, lang: "en".localized).send(signModel.self, then: updatePassResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    

    
}
