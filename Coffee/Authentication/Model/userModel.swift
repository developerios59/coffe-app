//
//  userModel.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation



// MARK: - UserModel
struct UserModel: Codable,CodableInit{
    let name, phone, email: String
    let address, lat, lng: String?
    let  code, userType: String
    let deviceID, deviceType: String
    let avatar: String
    let date, token: String

    enum CodingKeys: String, CodingKey {
        case name, phone, email, address, lat, lng, code
        case userType = "user_type"
        case deviceID = "device_id"
        case deviceType = "device_type"
        case avatar, date
        case token
    }
}
