//
//  signInModel.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - signModel
struct signModel : Codable ,CodableInit{
    let value, key: String
    let msg :String?
    let data: UserModel?
}

