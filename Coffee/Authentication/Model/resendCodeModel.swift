//
//  resendCodeModel.swift
//  cleanAppClient
//
//  Created by saleh on 5/5/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - resendCodeModel
struct resendCodeModel: Codable ,CodableInit {
    let value, key, msg : String
    let  data: String?
}
