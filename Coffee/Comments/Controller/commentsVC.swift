//
//  commentsVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

protocol commentsAdd {
    func loadData()
}

class commentsVC: UIViewController ,NVActivityIndicatorViewable {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var commentsStackView: UIStackView!
    @IBOutlet weak var emptyLB: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    var comments = [commentData]()
    
    
    var productId = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        tableView.delegate = self
        tableView.dataSource = self
        
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left"), for: .normal)
        
        getComments()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func getComments() {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.comments(lang: "en".localized, product_id: productId).send(commentModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                             self.tableView.isHidden = true
                            self.commentsStackView.isHidden = true
                            self.emptyLB.isHidden = false
                        }else{
                             self.emptyLB.isHidden = true
                            self.tableView.isHidden = false
                             self.commentsStackView.isHidden = false
                            self.comments = model.data
                            self.tableView.reloadData()
                        }
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    

    @IBAction func didSelectRateAddBu(_ sender: Any) {
        let rateScene =  rateAddVC.instantiate(fromAppStoryboard: .Comments)
               rateScene.productId = self.productId
        rateScene.delegate = self
        self.present(rateScene, animated: true, completion: nil)
                              
    }
}


extension commentsVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as! commentCell
        cell.configureCell(data: self.comments[indexPath.row])
        return cell
    }
    
    
}

extension commentsVC : commentsAdd {
    func loadData() {
        getComments()
    }
}
