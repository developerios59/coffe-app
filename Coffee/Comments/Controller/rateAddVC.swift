//
//  rateAddVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Cosmos
import MOLH

class rateAddVC: UIViewController,NVActivityIndicatorViewable {
    
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var commentTextView: UITextView!
    
    var delegate : commentsAdd?
    var productId = ""

    var commentValidation = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        commentTextView.delegate = self
         commentTextView.textColor = #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1)
        commentTextView.layer.cornerRadius = 8
        commentTextView.text = "Write Comment".localized
        commentTextView.textAlignment = MOLHLanguage.currentAppleLanguage().contains("en") ? .left : .right
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
  
    @IBAction func didSelectRateAddBu(_ sender: Any) {
        let response = Validation.shared.validate(values: (ValidationType.emptycomment,commentValidation))
    
               switch response {
               case .success:
                   break
               case .failure(_, let message):
                   Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
                   return
               }
               
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.addComment(lang: "en".localized, comment: commentTextView.text!, product_id: productId, rate: String(Int(rateView.rating))).send(rateAddModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        self.delegate?.loadData()
                        self.dismiss(animated: true, completion: nil)
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
}

extension rateAddVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1) {
               textView.text = nil
                commentValidation = "Valid"
               textView.textColor = UIColor.black
           }
       }
       
       
       func textViewDidEndEditing(_ textView: UITextView) {
           if textView.text.isEmpty {
               textView.textColor = #colorLiteral(red: 0.6470588235, green: 0.6470588235, blue: 0.6470588235, alpha: 1)
            commentValidation = ""
               textView.text = "Write Comment".localized
           }
       }

}
