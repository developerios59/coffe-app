//
//  commentCell.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import Cosmos

class commentCell: UITableViewCell {

    @IBOutlet weak var userImg: UIImageView!
    @IBOutlet weak var userNameLB: UILabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var commentLB: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        userImg.layer.cornerRadius = 30
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func configureCell(data:commentData) {
        userImg.kf.setImage(with: URL(string: data.avatar))
        rateView.rating = (data.rate as NSString).doubleValue
        userNameLB.text = data.name
        commentLB.text = data.comment
        
    }

}
