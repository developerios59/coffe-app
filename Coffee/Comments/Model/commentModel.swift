//
//  commentModel.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - commentModel

struct commentModel: Codable ,CodableInit{
    let value, key: String
    let data: [commentData]
}

// MARK: - commentData
struct commentData: Codable,CodableInit {
    let id: Int
    let avatar: String
    let name, comment, rate: String
}
