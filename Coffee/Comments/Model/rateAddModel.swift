//
//  rateAddModel.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


// MARK: - rateAddModel
struct rateAddModel: Codable ,CodableInit{
    let value, key, data: String
}
