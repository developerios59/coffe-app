//
//  categoryVC.swift
//  Coffee
//
//  Created by Mustafa on 10/6/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class categoryVC: UIViewController ,NVActivityIndicatorViewable {

    
    @IBOutlet weak var backGroundView: UIView!
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryNameLB: UILabel!
    @IBOutlet weak var productsView: UIView!
    @IBOutlet weak var searchTF: UITextFieldX!
    @IBOutlet weak var tableView: IntrinsicTableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var emptyLB: UILabel!
    @IBOutlet weak var filterView: UIViewX!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var dismissBtn: UIButton!
    
    
  
    
    var categoryName = ""
    var categoryImage = ""
    var categoryId  = ""
    
    var type = ""
    
    var products = [productData]()
     var searchedproducts = [productData]()
   var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        tableView.dataSource = self
        tableView.delegate = self
        scrollView.delegate = self
        
        filterBtn.imageView?.contentMode = .center
        productsView.layer.maskedCorners = [.layerMinXMinYCorner , .layerMaxXMinYCorner]
        productsView.layer.cornerRadius = 30
        categoryNameLB.text = categoryName
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right-small" : "left-small"), for: .normal)
        backBtn.layer.cornerRadius = backBtn.frame.height / 2
        searchView.layer.cornerRadius = searchView.frame.height / 2
         searchView.layer.borderWidth = 1
        searchView.layer.borderColor = Helper.standard.hexStringToUIColor(hex: "BFBFBF").cgColor
        
        self.categoryImg.kf.setImage(with: URL(string: categoryImage))
        let imagetap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        categoryImg.addGestureRecognizer(imagetap)
       getCategoryProducts(Id: categoryId, type: type)
        
        searchTF.addTarget(self, action: #selector(textFieldDidChange(_:)),
        for: .editingChanged)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        let iVC = FullImageVC()
        iVC.imageUrl = self.categoryImage
    
        self.navigationController?.pushViewController(iVC, animated: true)
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if !textField.text!.isEmpty {
            searchedproducts = products.filter({$0.name.contains(textField.text!)})
            searching = true
            self.tableView.isHidden = searchedproducts.isEmpty
            self.emptyLB.isHidden = !searchedproducts.isEmpty
            self.tableView.reloadData()
        }else {
            searching = false
            self.tableView.isHidden = false
            self.tableView.reloadData()
        }
    }
    
   
    
    func getCategoryProducts (Id:String,type:String) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.products(lang: "en".localized, type_id: Id, type: type).send(productModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            self.emptyLB.isHidden = false
                            self.tableView.isHidden = true
                        }else{
                            self.emptyLB.isHidden = true
                            self.tableView.isHidden = false
                            self.products = model.data
                            self.tableView.reloadData()
                        }
                    }
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    @IBAction func didSelectFilterBu(_ sender: Any) {
        filterView.isHidden = !filterView.isHidden
        dismissBtn.isHidden = !dismissBtn.isHidden
    }
    
    @IBAction func didSelectHighPriceSortBu(_ sender: Any) {
        filterView.isHidden = true
        dismissBtn.isHidden = true
        filter(sort: "max")
    }
    
    
    @IBAction func didSelectLowPriceSortBu(_ sender: Any) {
        filterView.isHidden = true
        dismissBtn.isHidden = true
        filter(sort: "min")
        
    }
    
    @IBAction func didSelecFiltertRemoveBu(_ sender: Any) {
        filterView.isHidden = true
        dismissBtn.isHidden = true
    }
    
    func filter(sort:String) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.filterProducts(lang: "en".localized, type_id: categoryId, type: type, price: sort).send(productModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            self.emptyLB.isHidden = false
                            self.tableView.isHidden = true
                        }else{
                            self.emptyLB.isHidden = true
                            self.tableView.isHidden = false
                            self.products = model.data
                            self.tableView.reloadData()
                        }
                    }
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
}


extension categoryVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.searching ? searchedproducts.count : products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "productCell", for: indexPath) as! productCell
        cell.configureCell(data: self.searching ? self.searchedproducts[indexPath.row] :  self.products[indexPath.row])
        cell.addAction = { [unowned self] in
            let productScene =  productVC.instantiate(fromAppStoryboard: .Products)
            productScene.productImage = self.searching ? self.searchedproducts[indexPath.row].image : self.products[indexPath.row].image
            productScene.productId =  self.searching ? String(self.searchedproducts[indexPath.row].id) : String(self.products[indexPath.row].id)
            self.navigationController?.pushViewController(productScene, animated: true)
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let productScene =  productVC.instantiate(fromAppStoryboard: .Products)
        productScene.productImage = self.searching ? self.searchedproducts[indexPath.row].image : self.products[indexPath.row].image
        productScene.productId =  self.searching ? String(self.searchedproducts[indexPath.row].id) : String(self.products[indexPath.row].id)
        self.navigationController?.pushViewController(productScene, animated: true)
    }
}

