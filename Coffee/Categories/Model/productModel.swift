//
//  productModel.swift
//  Coffee
//
//  Created by Mustafa on 10/6/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation


struct productModel: Codable,CodableInit {
    let value, key: String
    let data: [productData]
}

// MARK: - productData
struct productData: Codable ,CodableInit {
    let id: Int
    let image: String
    let name, datumDescription, price: String

    enum CodingKeys: String, CodingKey {
        case id, image, name
        case datumDescription = "description"
        case price
    }
}
