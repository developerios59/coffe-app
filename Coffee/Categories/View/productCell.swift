//
//  productCell.swift
//  Coffee
//
//  Created by Mustafa on 10/6/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class productCell: UITableViewCell {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productNameLB: UILabel!
    @IBOutlet weak var productDetailsLB: UILabel!
    @IBOutlet weak var productPriceLB: UILabel!
    
    var addAction:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        productImg.layer.cornerRadius = 15
    }
    
    
    func configureCell(data:productData) {
        productImg.kf.setImage(with: URL(string: data.image))
        productNameLB.text = data.name
        productDetailsLB.text = data.datumDescription
        productPriceLB.text = data.price + " SAR".localized
    }

    @IBAction func didSelectAddBu(_ sender: Any) {
    addAction?()
    }
    
}
