//
//  contactModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - contactModel
struct contactModel: Codable,CodableInit {
    let value, key: String
    let msg:String?
    let data: [socialData]
}

// MARK: - socialData
struct socialData: Codable,CodableInit {
    let id: Int
    let name: String
    let link: String
}
