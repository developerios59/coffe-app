//
//  socialModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - socialModel
struct socialModel: Codable,CodableInit {
    let value, key: String
    let data: [socialData]
}
