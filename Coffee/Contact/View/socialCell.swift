//
//  socialCell.swift
//  Mazad
//
//  Created by saleh on 2/16/20.
//  Copyright © 2020 saleh. All rights reserved.
//

import UIKit

class socialCell: UICollectionViewCell {
    
    @IBOutlet weak var socialImg: UIImageView!
    
    
    var socialAction : (() -> ())?
    
    func configureCell(data:socialData) {
        
        let imagetap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        socialImg.addGestureRecognizer(imagetap)
        socialImg.image = UIImage(named: data.name)
    }
    
     @objc func handleTap(_ sender: UITapGestureRecognizer) {
        socialAction?()
    }
}
