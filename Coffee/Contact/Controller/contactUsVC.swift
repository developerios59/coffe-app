//
//  contactUsVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class contactUsVC: UIViewController,NVActivityIndicatorViewable     {
    
   
    @IBOutlet weak var nameTF: UITextFieldX!
    @IBOutlet weak var phoneTF: UITextFieldX!
    @IBOutlet weak var messageTF: UITextView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    
    let userId = Helper.standard.userId()
  
    var social = [socialData]()

    var message = ""
    
    // response Handle
    var messageSendResponse: HandleResponse<contactModel> {
        return {[weak self] (response) in
            self?.stopAnimating()
            switch response {
            case .failure(let error):
                // TODO: - Handle error as you want, printing isn't handling.
                print(error)
            case .success(let model):
                //  print(model)
                if model.value == "0" {
                    Helper.standard.displayAlert(title: "Error".localized, message: model.value, vc: self!)
                }else if model.value == "1" {
                    if self!.social.isEmpty {
                        self?.social = model.data
                        self?.collectionView.reloadData()
                    }else{
                        self?.setupUI()
                        Helper.standard.displayAlert(title: "Success".localized, message: model.msg!, vc: self!)
                    }
                }
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        ComplaintSend()
     
        setupUI()
        
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    func setupUI() {
        nameTF.delegate = self
             phoneTF.delegate = self
             messageTF.delegate = self
             collectionView.delegate = self
             collectionView.dataSource = self
             
        phoneTF.text = ""
         nameTF.text = ""
             messageTF.textColor = #colorLiteral(red: 0.1764705882, green: 0.1764705882, blue: 0.1764705882, alpha: 0.7)
             messageTF.text = "write message".localized
             
             self.tabBarController?.tabBar.isHidden = true
             self.tabBarController?.tabBar.isTranslucent = true
               backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
    }

    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didSelectSendBu(_ sender: Any) {
        ComplaintSend()
    }
    
    
    func ComplaintSend() {
        self.view.endEditing(true)
        // Validation
        if !self.social.isEmpty {
        let response = Validation.shared.validate(values: (ValidationType.emptyName, nameTF.text!),(ValidationType.phoneNo , phoneTF.text!),(ValidationType.emptymessage , message))
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        }
        // Api Request
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            
            UserRouter.contactUs(lang: "en".localized, name: nameTF.text!, phone: phoneTF.text!, message: messageTF.text!).send(contactModel.self, then: messageSendResponse)
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
}

extension contactUsVC  :UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
             ComplaintSend()
              return true
          }

}

extension contactUsVC : UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == #colorLiteral(red: 0.1764705882, green: 0.1764705882, blue: 0.1764705882, alpha: 0.7) {
            textView.text = nil
            message = "Valid"
            textView.textColor = UIColor.black
        }
    }
       
       
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = #colorLiteral(red: 0.1764705882, green: 0.1764705882, blue: 0.1764705882, alpha: 0.7)
            message = ""
            textView.text = "write message".localized
        }
    }

}


extension contactUsVC : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return social.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "socialCell", for: indexPath) as! socialCell
        
        cell.socialAction = { [unowned self] in
            
            if indexPath.row == 1 {
                if   let urlWhats = "whatsapp://send?phone=\(self.social[indexPath.row].link)&text=Hello".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                    if UIApplication.shared.canOpenURL(URL(string: urlWhats)! ) {
                        UIApplication.shared.open(URL(string: urlWhats)!, options: [:], completionHandler: nil)
                    } else {
                        if   let urlWhats = "https://wa.me/\(self.social[indexPath.row].link)".addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                            if UIApplication.shared.canOpenURL(URL(string: urlWhats)! ) {
                                UIApplication.shared.open(URL(string: urlWhats)!, options: [:], completionHandler: nil)
                            }
                        }
                    }
                }
            }else{
            if let url = self.social[indexPath.row].link.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                if (UIApplication.shared.canOpenURL(URL(string:url)!)) {
                    UIApplication.shared.open(URL(string:url)!, completionHandler: nil)
                }else{
                    Helper.standard.displayAlert(title: "", message: "there is no account".localized, vc: self)
                }
            }else{
                Helper.standard.displayAlert(title: "", message: "there is no account".localized, vc: self)
            }
            }
        }
        
        cell.configureCell(data: social[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       let width = collectionView.frame.width - 10
        return CGSize(width: Int(width) / social.count, height: 30)
    }
}
