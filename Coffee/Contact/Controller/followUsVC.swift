//
//  followUsVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/31/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class followUsVC: UIViewController,NVActivityIndicatorViewable {

    
     @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var backBtn: UIButton!
    
     let userId = Helper.standard.userId()
     var socialsData = [socialData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        
         backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        self.tabBarController?.tabBar.isHidden = true
             self.tabBarController?.tabBar.isTranslucent = true
        
        getSocialData()
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
         self.navigationController?.popViewController(animated: true)
         
     }
       
    
   
                
       
       
    
   func getSocialData() {
       if Reachability.isConnectedToNetwork(){
           self.startAnimating()
        UserRouter.followUs.send(socialModel.self) { (response) in
            self.stopAnimating()
            switch response {
            case .failure(let error):
                // TODO: - Handle error as you want, printing isn't handling.
                print(error)
            case .success(let model):
                //  print(model)
                if !model.data.isEmpty {
                    self.socialsData = model.data
                    self.collectionView.reloadData()
                }

            }
        }


       }else{
           Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
       }
   }
   
}


extension followUsVC : UICollectionViewDelegate , UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return socialsData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "socialCell", for: indexPath) as! socialCell
        
        cell.socialAction = { [unowned self] in
            if let url = self.socialsData[indexPath.row].link.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                if (UIApplication.shared.canOpenURL(URL(string:url)!)) {
                    UIApplication.shared.open(URL(string:url)!, completionHandler: nil)
                }else{
                    Helper.standard.displayAlert(title: "", message: "there is no account".localized, vc: self)
                }
            }else{
                Helper.standard.displayAlert(title: "", message: "there is no account".localized, vc: self)
            }
        }
        
        cell.configureCell(data: socialsData[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
       let width = collectionView.frame.width - 10
        return CGSize(width: Int(width) /  socialsData.count , height: 30)
    }
}
