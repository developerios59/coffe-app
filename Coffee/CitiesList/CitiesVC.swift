//
//  CitiesVC.swift
//  Coffee
//
//  Created by mohamed sayed on 12/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CitiesVC: UIViewController,NVActivityIndicatorViewable {
    
    let cellID = "CellID"
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var cityLable: UILabel!
    @IBOutlet weak var citiesNameTable: UITableView!
    
    var delegate : GetShippingCost!
    
    var cities = [City]() {
        didSet {
            self.citiesNameTable.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewConfigration()
        getCities()
        actionButtons()
        setUpView()
    }
    
    
    
}
