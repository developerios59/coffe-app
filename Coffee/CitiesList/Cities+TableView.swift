//
//  Cities+TableView.swift
//  Coffee
//
//  Created by mohamed sayed on 12/14/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH


protocol GetShippingCost {
    func theCost(value:Double)
    func cityName(Name:String,cityID:Int)
    func selectCitiy(Select:Bool)

}



extension CitiesVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cities.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID)
        cell?.textLabel?.textAlignment = .center

        if MOLHLanguage.currentAppleLanguage().contains("en") {
            cell?.textLabel?.text = cities[indexPath.item].nameEn
        }else{
            cell?.textLabel?.text = cities[indexPath.item].name
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate.selectCitiy(Select: true)
        let cityID = cities[indexPath.item]
        if MOLHLanguage.currentAppleLanguage().contains("en") {
            self.delegate.cityName(Name: cityID.nameEn ?? "", cityID: cityID.id ?? 0 )
        }else{
            self.delegate.cityName(Name: cityID.name ?? "", cityID: cityID.id ?? 0 )
        }
        CoffeeClient.getShippingCost(cityID: cityID.id ?? 0) { [weak self] (response, error) in
            guard let self = self else {return}
            if response?.isSuccess != true {
                print("the error data is \(response?.messageEn ?? "" )")
            }else{
                self.delegate?.theCost(value: Double(response?.resultData?.totalCost ?? 0) )
                self.dismiss(animated: true , completion: nil)
            }
        }
    }
}
