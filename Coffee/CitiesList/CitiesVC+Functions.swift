//
//  CitiesVC+Functions.swift
//  Coffee
//
//  Created by mohamed sayed on 12/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit


extension CitiesVC {

    func tableViewConfigration(){
        citiesNameTable.delegate = self
        citiesNameTable.dataSource = self
        citiesNameTable.register(UITableViewCell.self, forCellReuseIdentifier: cellID)
    }
    
    
    func setUpView(){
        cityLable.text = "Cities".localized
    }
    
    func actionButtons(){
        cancelButton.addTarget(self, action: #selector(cancelButtonTapped), for: .touchUpInside)
        
    }
    
    @objc func cancelButtonTapped(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func getCities(){
        self.startAnimating()
        CoffeeClient.getCitiesAPI { (response, error) in
            self.stopAnimating()
            if response?.isSuccess != true {
                print("the error message is \(response?.messageEn ?? "" )")
            }else{
                if let countryList =  response?.resultData?.countryCityList {
                    for city in countryList {
                        self.cities.append(contentsOf: city.cities ?? [])
                    }
                    
                }
                
            }
            
        }
    }
    
}
