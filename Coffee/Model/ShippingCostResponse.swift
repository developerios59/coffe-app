//
//  ShippingCostResponse.swift
//  Coffee
//
//  Created by mohamed sayed on 12/14/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct ShippingCostResponse: Codable {
    let httpStatusCode: Int?
    let isSuccess: Bool?
    let messageAr, messageEn: String?
    let resultData: CostData?
}

// MARK: - ResultData
struct CostData: Codable {
    let totalCost: Int?
    let totalCostCurrency: String?
//    let paymentMethod, shippingMethod: Method?
}

//// MARK: - Method
//struct Method: Codable {
//    let id: Int?
//    let nameAr, nameEn: String?
//}
