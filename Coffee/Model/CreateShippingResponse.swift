//
//  CreateShippingResponse.swift
//  Coffee
//
//  Created by mohamed sayed on 12/14/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct CreateShippingResponse: Codable {
    let httpStatusCode: Int?
    let isSuccess: Bool?
    let messageAr, messageEn: String?
}
