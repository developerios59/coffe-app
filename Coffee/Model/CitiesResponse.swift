//
//  CitiesResponse.swift
//  Coffee
//
//  Created by mohamed sayed on 12/14/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct CitiesResponse: Codable {
    let httpStatusCode: Int?
    let isSuccess: Bool?
    let messageAr, messageEn: String?
    let resultData: CitiesData?
}

struct CitiesData: Codable {
    let countryCityList: [CountryCityList]?
    let paymentMethodList: [List]?
    let servicesList: [ServicesList]?
    let shipmentContentTypeList, shippingMethodsList: [List]?
}

struct CountryCityList: Codable {
    let id: Int?
    let name, alpha2Code: String?
    let cities: [City]?
}

struct City: Codable {
    let id: Int?
    let name: String?
    let nameEn: String?
    let countryID: Int?
    let isCoveredByQuick: Bool?

    enum CodingKeys: String, CodingKey {
        case id, name, nameEn
        case countryID = "countryId"
        case isCoveredByQuick
    }
}

// MARK: - List
struct List: Codable {
    let id: Int?
    let nameAr, nameEn: String?
}

// MARK: - ServicesList
struct ServicesList: Codable {
    let id: Int?
    let nameAR, nameEN: String?
}
