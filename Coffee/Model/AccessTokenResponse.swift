//
//  Model.swift
//  Coffee
//
//  Created by mohamed sayed on 12/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//


import Foundation

struct AccessTokenResponse: Codable {
    let httpStatusCode: Int?
    let isSuccess: Bool?
    let messageAr, messageEn: String?
    let resultData: ResultData?
}

struct ResultData: Codable {
    let accessToken, tokenType, accessTokenExpires, refreshToken: String?
    let refreshTokenExpires: String?

    enum CodingKeys: String, CodingKey {
        case accessToken = "access_token"
        case tokenType = "token_type"
        case accessTokenExpires = "access_token_expires"
        case refreshToken = "refresh_token"
        case refreshTokenExpires = "refresh_token_expires"
    }
}


