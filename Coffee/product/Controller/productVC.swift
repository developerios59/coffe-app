//
//  productVC.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Cosmos
import MOLH
import RealmSwift
import ImageSlideshow

class productVC: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var imageSlider: ImageSlideshow!
     @IBOutlet var scrollView: UIScrollView!
    
    @IBOutlet weak var shareBtn: UIButton!
    
    @IBOutlet weak var favoriteBtn: UIButton!
    
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var productNameLB: UILabel!
    @IBOutlet weak var rateLB: UnderlinedLabel!
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var descriptionLB: UILabel!
    @IBOutlet weak var quantityLB: UILabel!
    @IBOutlet weak var priceLB: UILabel!
    

    let userId = Helper.standard.userId()
    
    
    
    var productImage = ""
    var productId = ""
    
    var shareLink = ""
    
    var unitPrice = 0.0
    var productCount = 1.0
    
    var imageSliderArr = [KingfisherSource]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
       getProductDetails(productId: productId)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    func setupUI() {
        self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        shareBtn.layer.cornerRadius = shareBtn.frame.height / 2
        backBtn.layer.cornerRadius = shareBtn.frame.height / 2
        favoriteBtn.layer.cornerRadius = favoriteBtn.frame.height / 2
       
        self.quantityLB.text = String(self.productCount)
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right-small" : "left-small"), for: .normal)
        
        
        imageSlider.layer.cornerRadius = 0
        imageSlider.delegate = self
        imageSlider.slideshowInterval = 3.0
        imageSlider.pageIndicator = nil
        imageSlider.contentScaleMode = .scaleToFill
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTap))
        imageSlider.addGestureRecognizer(gestureRecognizer)
        
        
        
        let ratetap = UITapGestureRecognizer(target: self, action: #selector(rateTap(_:)))
        rateLB.addGestureRecognizer(ratetap)
    }
    
    
    
    @objc func didTap() {
        imageSlider.presentFullScreenController(from: self)
    }
    
     @objc func rateTap(_ sender: UITapGestureRecognizer) {
        let commentsScene =  commentsVC.instantiate(fromAppStoryboard: .Comments)
        commentsScene.productId = self.productId
        self.navigationController?.pushViewController(commentsScene, animated: true)
                       
    }
    
    func getProductDetails(productId :String) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.productDetails(lang: "en".localized, product_id: productId).send(productDetailsModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        self.shareLink = model.data.share
                        self.productNameLB.text = model.data.name
                        self.rateLB.text = "rates ".localized + "(\(model.data.comments))"
                        self.rateView.rating = Double(model.data.rate)
                        self.descriptionLB.text = model.data.dataDescription
                        self.priceLB.text = model.data.price + " SAR".localized
                        self.unitPrice = (model.data.price as NSString).doubleValue
                        
                        self.favoriteBtn.setImage(UIImage(named: model.data.isFavorite == 1 ? "heart" : "heart-empty"), for: .normal)
                        if self.imageSliderArr.isEmpty {
                            if model.data.images.isEmpty {
                                self.imageSliderArr.append(KingfisherSource(urlString:  model.data.image)!)
                                
                            }else{
                                for image in model.data.images {
                                    self.imageSliderArr.append(KingfisherSource(urlString: image)!)
                                }
                            }
                            self.imageSlider.setImageInputs(self.imageSliderArr)
                            self.imageSlider.reloadInputViews()
                        }
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        contentViewWidth.constant = view.frame.width
//        contentViewHeight.constant = 1200
//    }
    
    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didSelectShareBu(_ sender: Any) {
               if let appURL = NSURL(string: shareLink) {
                   let objectsToShare: [Any] = [appURL]
                   let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                   self.present(activityVC, animated: true, completion: nil)
               }
           }

    
    
    @IBAction func didSelectFavoriteBu(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.favorite(lang: "en".localized, product_id: self.productId).send(favoriteAddModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        self.favoriteBtn.setImage(UIImage(named: model.data == 1 ? "heart" : "heart-empty"), for: .normal)
                    }
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    @IBAction func didSelectAddBu(_ sender: Any) {
        self.productCount += 1
        self.quantityLB.text = String(self.productCount)
        self.priceLB.text = String(self.productCount * self.unitPrice)
    }
    
    @IBAction func didSelectMinusBu(_ sender: Any) {
        self.productCount -= productCount != 1 ? 1 : 0
        self.quantityLB.text = String(self.productCount)
        self.priceLB.text = String(self.productCount * self.unitPrice)
    }
    
    @IBAction func didSelectCartAddBu(_ sender: Any) {
        
             let number = arc4random_uniform(10000)
            let cartItem = CartItem()
                cartItem.id = Int(number)
                cartItem.orderQuantity =  Int(self.productCount)
                cartItem.orderName = self.productNameLB.text!
                cartItem.orderdescription = self.descriptionLB.text!
        cartItem.orderPrice = Double(self.unitPrice)
                cartItem.orderImage = self.productImage
                cartItem.productID = self.productId
                cartItem.count  = Double(self.productCount)
                cartItem.userId = self.userId
//               cartItem.totlalPriceForQuantity = String(self.productCount * self.unitPrice)
            do {
                let realm = try Realm()
                 let items = realm.objects(CartItem.self).filter("productID = '\(self.productId)'")
                
                try realm.write {
                    if !items.isEmpty {
                        let id =  items.first!.id
                        cartItem.id = id
                        realm.add(cartItem, update: .all)
                    }else{
                        realm.add(cartItem)
                    }
                }
                
            let cartScene =  cartVC.instantiate(fromAppStoryboard: .Cart)
            self.navigationController?.pushViewController(cartScene, animated: true)
            
            }catch {
                print(error)
            }
    }
    
    
}



extension productVC : ImageSlideshowDelegate {
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {

    }
}



