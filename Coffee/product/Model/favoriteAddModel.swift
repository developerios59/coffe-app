//
//  favoriteAddModel.swift
//  Coffee
//
//  Created by Mustafa on 10/17/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - favoriteAddModel
struct favoriteAddModel: Codable ,CodableInit{
    let value, key, msg: String
    let data: Int
}
