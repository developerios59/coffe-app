//
//  productDetailsModel.swift
//  Coffee
//
//  Created by Mustafa on 10/6/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct productDetailsModel: Codable ,CodableInit{
    let value, key: String
    let data: productDetailsData
}

// MARK: - productDetailsData
struct productDetailsData: Codable ,CodableInit{
    let id: Int
    let images : [String]
    let image: String
    let share: String
    let rate :Double
    let  isFavorite, comments: Int
    let name, dataDescription, price: String

    enum CodingKeys: String, CodingKey {
        case id, image, share, rate, images
        case isFavorite = "is_favorite"
        case comments, name
        case dataDescription = "description"
        case price
    }
}
