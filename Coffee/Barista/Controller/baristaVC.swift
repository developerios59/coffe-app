//
//  baristaVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class baristaVC: UIViewController ,NVActivityIndicatorViewable{

    @IBOutlet weak var baristaCollectionView: UICollectionView!
    @IBOutlet weak var welcomeImage: UIImageView!
    
    var baristas = [baristaData]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        baristaCollectionView.delegate = self
        baristaCollectionView.dataSource = self
        
        getBarista()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.5) {
            UIView.animate(withDuration: 0.6) {
                self.welcomeImage.alpha = 0
            }
        }

    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    override func viewWillAppear(_ animated: Bool) {
       self.tabBarController?.tabBar.isHidden = false
        self.tabBarController?.tabBar.isTranslucent = false
        
    }

    func getBarista() {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.barista(lang: "en".localized).send(baristaModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        self.baristas = model.data
                        self.baristaCollectionView.reloadData()
                        
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }

}


extension baristaVC : UICollectionViewDataSource ,UICollectionViewDelegate ,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return baristas.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "baristaCell", for: indexPath) as! baristaCell
        cell.configureCell(data: self.baristas[indexPath.row])
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width - 20
        return CGSize(width: width / 2, height:  300)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let  baristaDetailsScene = baristaDetailsVC.instantiate(fromAppStoryboard: .Barista)
        baristaDetailsScene.baristaId = String(self.baristas[indexPath.row].id)
        self.navigationController?.pushViewController(baristaDetailsScene, animated: true)
    }
    
}
