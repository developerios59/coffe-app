//
//  baristaDetailsVC.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import MOLH

class baristaDetailsVC: UIViewController ,NVActivityIndicatorViewable{
    
    @IBOutlet weak var baristaNameLB: UILabel!
    @IBOutlet weak var baristaImg: UIImageView!
    @IBOutlet weak var baristaDescriptionLB: UILabel!
    @IBOutlet weak var similarProductsCollectionView: UICollectionView!
    @IBOutlet weak var baristaChoisesLB: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    var baristaId = ""
    
    var baristaProducts = [Product]()


    override func viewDidLoad() {
        super.viewDidLoad()
        similarProductsCollectionView.dataSource = self
        similarProductsCollectionView.delegate = self
        
         backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right-small" : "left-small" ), for: .normal)
        
      getBaristaDetails(baristaId: baristaId)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .default
       }
       
    
    
    override func viewWillAppear(_ animated: Bool) {
         self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
    }
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    func getBaristaDetails(baristaId : String) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.baristaDetails(lang: "en".localized, barista_id: baristaId).send(barisatDetailModel.self) { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        self.baristaNameLB.text = model.data.name
                        self.baristaImg.kf.setImage(with: URL(string: model.data.image))
                        self.baristaDescriptionLB.text = model.data.dataDescription
                        self.baristaProducts = model.products
                        self.baristaChoisesLB.isHidden = model.products.isEmpty
                         self.similarProductsCollectionView.isHidden = model.products.isEmpty
                        self.similarProductsCollectionView.reloadData()
                        
                    }
                    
                }
            }
            
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
    
    
    
}

extension baristaDetailsVC : UICollectionViewDataSource , UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return baristaProducts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "baristaProductCell", for: indexPath) as! baristaProductCell
        cell.configureCell(data: baristaProducts[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           let height = collectionView.frame.height - 20
           return CGSize(width: 150, height:height)
       }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let productScene =  productVC.instantiate(fromAppStoryboard: .Products)
            productScene.productImage = self.baristaProducts[indexPath.row].image
            productScene.productId =  String(self.baristaProducts[indexPath.row].id)
        self.navigationController?.pushViewController(productScene, animated: true)
    }
}
