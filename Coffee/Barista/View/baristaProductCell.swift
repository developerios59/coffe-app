//
//  baristaProductCell.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import Cosmos

class baristaProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productImg: UIImageViewX!
      @IBOutlet weak var productNameLB: UILabel!
      @IBOutlet weak var productDescriptionLB: UILabel!
      @IBOutlet weak var productRatingView: CosmosView!
      @IBOutlet weak var priceLB: UILabel!
      
      
      func configureCell(data:Product) {
        productImg.kf.setImage(with: URL(string: data.image))
        productNameLB.text = data.name
        productDescriptionLB.text = data.productDescription
        productRatingView.rating = Double(data.rate)
        priceLB.text = data.price + " SAR".localized
          
      }
    
}
