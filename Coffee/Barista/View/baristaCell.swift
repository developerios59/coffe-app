//
//  baristaCell.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class baristaCell: UICollectionViewCell {
    
    
    @IBOutlet weak var baristaImg: UIImageView!
    @IBOutlet weak var baristaNameLB: UILabel!
    
    func configureCell(data:baristaData) {
        baristaImg.kf.setImage(with: URL(string: data.image))
        baristaNameLB.text = data.name
    }
    
}
