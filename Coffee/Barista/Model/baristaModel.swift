//
//  baristaModel.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - baristaModel
struct baristaModel: Codable,CodableInit {
    let value, key: String
    let data: [baristaData]
}

// MARK: - baristaData
struct baristaData: Codable ,CodableInit{
    let id: Int
    let name: String
    let image: String
}
