//
//  barisatDetailModel.swift
//  Coffee
//
//  Created by Mustafa on 10/7/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - barisatDetailModel
struct barisatDetailModel: Codable ,CodableInit{
    let value, key: String
    let data: barisatDetailData
    let products: [Product]
}

// MARK: - barisatDetailData
struct barisatDetailData: Codable,CodableInit {
    let image: String
    let name, dataDescription: String

    enum CodingKeys: String, CodingKey {
        case image, name
        case dataDescription = "description"
    }
}

// MARK: - Product
struct Product: Codable ,CodableInit{
    let id: Int
    let image: String
    let name, productDescription, price: String
    let rate: Int
    
    enum CodingKeys: String, CodingKey {
        case id, image, name
        case productDescription = "description"
        case price, rate
    }
}
