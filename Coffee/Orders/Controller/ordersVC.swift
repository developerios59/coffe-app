//
//  ordersVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import BetterSegmentedControl
import NVActivityIndicatorView
import MOLH

class ordersVC: UIViewController,NVActivityIndicatorViewable {

    @IBOutlet weak var segmentedControl: BetterSegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLB: UILabel!
    @IBOutlet weak var backBtn: UIButton!
    
    let userId = Helper.standard.userId()
    
    var orders = [ordersData]()
    
    var orderStatus = "current"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        tableView.delegate = self
        tableView.dataSource = self
        
        segmentedControl.segments = LabelSegment.segments(withTitles: ["Current Orders".localized, "Completed Orders".localized],
        normalFont: UIFont(name: "JF Flat", size: 14.0)!,
        normalTextColor:  #colorLiteral(red: 0.1294117647, green: 0.1294117647, blue: 0.1294117647, alpha: 1) ,
        selectedFont: UIFont(name: "JF Flat", size: 14.0)!,
        selectedTextColor: .white)
        segmentedControl.indicatorViewBackgroundColor = #colorLiteral(red: 0.3073959947, green: 0.2393867671, blue: 0.2604644895, alpha: 1)
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
          self.navigationController?.popViewController(animated: true)
      }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
          self.tabBarController?.tabBar.isHidden = true
               self.tabBarController?.tabBar.isTranslucent = true
        self.navigationController?.navigationBar.isHidden = false
         getOrders(orderStatus: orderStatus)
    }
    

    @IBAction func didSelectBackBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
    @IBAction func didSelectSegmentedControl(_ sender: Any) {
        switch segmentedControl.index {
        case 0 :
          // MARK:- Current Orders
            orderStatus = "current"
          
        case 1 :
          // MARK:- completed Orders
            orderStatus = "completed"
        default:
            orderStatus = "current"
        }
      
      getOrders(orderStatus: orderStatus)
        
    }
    
    
    func getOrders(orderStatus:String) {
        print(orderStatus)
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.orders(lang: "en".localized, status: orderStatus).send(ordersModel.self)  { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data.isEmpty {
                            self.tableView.isHidden = true
                            self.emptyLB.isHidden = false
                        }else{
                            self.tableView.isHidden = false
                            self.emptyLB.isHidden = true
                            self.orders = model.data
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
   

}


extension ordersVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderCell", for: indexPath) as! orderCell
        cell.configureCell(data: self.orders[indexPath.row])
        return cell
    }
    
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        let orderDetailsScene =  ordersProductsVC.instantiate(fromAppStoryboard: .Orders)
        orderDetailsScene.orderId = String(self.orders[indexPath.row].id)
        orderDetailsScene.isFinishedOrders = orderStatus == "completed" ? true : false
        self.navigationController?.pushViewController(orderDetailsScene, animated: true)
    }
    
}
