//
//  ordersProductsVC.swift
//  goldenBrickAgent
//
//  Created by saleh on 9/9/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import MOLH
import NVActivityIndicatorView

class ordersProductsVC: UIViewController ,NVActivityIndicatorViewable{

   
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLB: UILabel!
    @IBOutlet weak var paymentBtn: UIButton!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var followUpBtn: UIButtonX!
    
    let userId = Helper.standard.userId()
    
    var products = [orderProductsData]()
    
    var orderId = ""
    var orderStatus = ""
    var total = ""
    
    var isFinishedOrders = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        followUpBtn.isHidden = isFinishedOrders
        customNavBar()
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
             return .lightContent
       }
       
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          self.tabBarController?.tabBar.isHidden = true
            self.tabBarController?.tabBar.isTranslucent = true
        self.navigationController?.navigationBar.isHidden = false
         getProducts(orderId: orderId)
    }
    
    
    func customNavBar() {
           let mainLB = UIButton(type: .custom)
           mainLB.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        mainLB.setTitle("Products".localized, for: .normal)
           mainLB.titleLabel?.font = UIFont(name: "JF Flat", size: 18)
           
           let mainLBbarButton = UIBarButtonItem(customView: mainLB)
           
           self.navigationItem.hidesBackButton = true
           self.navigationItem.setLeftBarButtonItems([mainLBbarButton], animated: true)

           
           let backBtn = UIButton(type: .custom)
                  backBtn.addTarget(self, action:#selector(didSelectBackBu), for:.touchUpInside)
                  backBtn.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
      backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right-arrow" : "right"  ), for: .normal)
           let notificationbarButton = UIBarButtonItem(customView: backBtn)
           
            self.navigationItem.setRightBarButtonItems([notificationbarButton], animated: true)
          
       }

       @objc  func didSelectBackBu() {
        self.navigationController?.popViewController(animated: true)
       }
       
    func getProducts(orderId:String) {
        if Reachability.isConnectedToNetwork(){
            self.startAnimating()
            UserRouter.orderProducts(lang: "en".localized, order_id: orderId).send(orderProductsModel.self)  { (response) in
                self.stopAnimating()
                switch response {
                case .failure(let error):
                    // TODO: - Handle error as you want, printing isn't handling.
                    print(error)
                case .success(let model):
                    print(model)
                    if model.value == "0" {
                        
                    }else if model.value == "1" {
                        if model.data!.isEmpty {
                            self.tableView.isHidden = true
                            self.emptyLB.isHidden = false
                        }else{
                            self.tableView.isHidden = false
                            self.emptyLB.isHidden = true
                            self.products = model.data!
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }else{
            Helper.standard.displayAlert(title: "Error".localized, message: "Internet Connection is not Available!".localized, vc: self)
        }
    }
   
    
    @IBAction func didSelectDemandFollowUpBu(_ sender: Any) {
        let orderTrackScene = orderFollowUpVC.instantiate(fromAppStoryboard: .OrderFollowUp)
        orderTrackScene.orderId = self.orderId
        self.navigationController?.pushViewController(orderTrackScene, animated: true)
    }

}



extension ordersProductsVC : UITableViewDataSource , UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "orderProductsCell", for: indexPath) as! orderProductsCell
        
        cell.configureCell(data: self.products[indexPath.row])
        
        return cell
    }
    
    
}
