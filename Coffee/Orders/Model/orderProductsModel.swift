//
//  orderProductsModel.swift
//  Coffee
//
//  Created by Mustafa on 10/17/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

struct orderProductsModel: Codable ,CodableInit{
    let value, key: String
    let msg:String?
    let data: [orderProductsData]?
}

// MARK: - orderProductsData
struct orderProductsData: Codable ,CodableInit{
    let id: Int
    let name, category, datumDescription, price: String
    let image: String

    enum CodingKeys: String, CodingKey {
        case id, name, category
        case datumDescription = "description"
        case price, image
    }
}
