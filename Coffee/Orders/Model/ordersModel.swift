//
//  ordersModel.swift
//  goldenBrickAgent
//
//  Created by saleh on 9/3/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - ordersModel
struct ordersModel: Codable,CodableInit {
    let value, key: String
    let data: [ordersData]
}

// MARK: - ordersData
struct ordersData: Codable ,CodableInit{
    let id: Int
    let date, payment: String
}


