//
//  orderDeleteModel.swift
//  goldenBrickAgent
//
//  Created by Mustafa on 9/13/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - orderDeleteModel
struct orderDeleteModel: Codable ,CodableInit{
    let value, key: String
    let msg,data: String?
}
