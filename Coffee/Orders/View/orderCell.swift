//
//  orderCell.swift
//  goldenBrickAgent
//
//  Created by saleh on 8/27/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class orderCell: UITableViewCell {


    @IBOutlet weak var orderDateLB: UILabel!
    @IBOutlet weak var orderNumberLB: UILabel!
    @IBOutlet weak var paymentMethodLB: UILabel!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCell(data:ordersData) {
        orderDateLB.text = data.date
        orderNumberLB.text = String(data.id)
        paymentMethodLB.text = data.payment.localized
    }

}
