//
//  orderProductsCell.swift
//  Coffee
//
//  Created by Mustafa on 10/17/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit

class orderProductsCell: UITableViewCell {

    @IBOutlet weak var categoryNameLB: UILabel!
    @IBOutlet weak var productNameLB: UILabel!
    @IBOutlet weak var productDescriptionLB: UILabel!
    @IBOutlet weak var productPriceLB: UILabel!
    @IBOutlet weak var productImg: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func configureCell(data:orderProductsData) {
        self.productImg.kf.setImage(with: URL(string: data.image))
        self.categoryNameLB.text = data.category
        self.productNameLB.text = data.name
        self.productDescriptionLB.text = data.datumDescription
        self.productPriceLB.text = data.price + " SAR".localized
    }

}
