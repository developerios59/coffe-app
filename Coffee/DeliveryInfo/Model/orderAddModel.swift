//
//  orderAddModel.swift
//  Coffee
//
//  Created by Mustafa on 10/16/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import Foundation

// MARK: - orderAddModel
struct orderAddModel: Codable ,CodableInit{
    let value, key: String
    let msg : String?
    let data: Int?
}
