//
//  deliveryInfoVC.swift
//  Coffee
//
//  Created by Mustafa on 10/1/20.
//  Copyright © 2020 Mustafa. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import RealmSwift
import MOLH
import GoogleMaps

class deliveryInfoVC: UIViewController ,NVActivityIndicatorViewable {
    
    @IBOutlet weak var locationSelectTF: UITextFieldX!
    @IBOutlet weak var timeTF: UITextFieldX!
    @IBOutlet weak var dateTF: UITextFieldX!
    @IBOutlet weak var backBtn: UIButton!
    
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var shipinngDetailsStack: UIStackView!
    
    @IBOutlet weak var deliveryLable: UILabel!
    @IBOutlet weak var taxLable: UILabel!
    @IBOutlet weak var totalLable: UILabel!
    
    @IBOutlet weak var deliveryValueLable: UILabel!
    @IBOutlet weak var taxValueLable: UILabel!
    @IBOutlet weak var totalValueLable: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    var locationManager = CLLocationManager()
    var marker = GMSMarker()
    
    var mapAddress: String?
    var lat: String?
    var lng: String?
    
    var cityID = Int()
    var deliveryValueFromApi = Double()
    let Formatter = DateFormatter()
    var components = DateComponents()
    
    var cartOrders = [cartModel]()
    var paymentMethodId = ""
    var address = ""
    var latitude = ""
    var longtitude = ""
    var delivery = 0
    var tax = ""
    var total = 0
    
    var time = ""
    var date = ""
    
    let userDefaults = UserDefaults.standard
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        self.locationManager.requestWhenInUseAuthorization()
        self.mapView.delegate = self
        setUpviewData()
        shipinngDetailsStack.isHidden = true
        mapView.isHidden = true
        locationSelectTF.delegate = self
        purchaseButton.isEnabled = false
        timeTF.delegate = self
        dateTF.delegate = self
        setupUI () 
        self.tabBarController?.tabBar.isHidden = true
        self.tabBarController?.tabBar.isTranslucent = true
        
        backBtn.setImage(UIImage(named: MOLHLanguage.currentAppleLanguage().contains("en") ? "right" : "left" ), for: .normal)
        
    }
    
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    func setupUI () {
        self.components = Calendar.current.dateComponents([.year , .month , .day ], from: Date())
        Formatter.dateFormat = "yyyy/MM/dd"
        self.date = Formatter.string(from: Date())
        print("date : \(date)")
        Formatter.dateFormat = "hh:mm a"
        self.time = Formatter.string(from: Date())
        print("time : \(time)")
    }
    
    
    @IBAction func didSelectDismissBu(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func didSelectPurchaseBu(_ sender: Any) {
        //MARK:Creat Shippment
        
        let response = Validation.shared.validate(values:(ValidationType.emptyAddress , self.address),(ValidationType.emptyDate , self.date),(ValidationType.emptyTime , self.time))
        
        switch response {
        case .success:
            break
        case .failure(_, let message):
            Helper.standard.displayAlert(title: "Error".localized, message: message, vc: self)
            return
        }
        var jsonData = Data()
        do {
            jsonData =  try JSONEncoder().encode(self.cartOrders)
        }catch{
            print(error)
        }
        
        let userName = userDefaults.string(forKey: "userName")
        let userPhone = userDefaults.string(forKey: "userMobileNumber")
        
        guard let ordertransformed = String(data: jsonData, encoding: .utf8) else { return  }
    let paymentMethodScene =  paymentMethodVC.instantiate(fromAppStoryboard: .PaymentMethods)
        paymentMethodScene.lat = self.latitude
        paymentMethodScene.lng = self.longtitude
        paymentMethodScene.delivery = Int(self.deliveryValueFromApi)
        paymentMethodScene.cityID = "\(cityID)"
        paymentMethodScene.userName = userName ?? ""
        paymentMethodScene.phoneNumber = userPhone ?? ""

        paymentMethodScene.time = self.time
        paymentMethodScene.date = self.date
        paymentMethodScene.address = self.address
        paymentMethodScene.orderdCart = ordertransformed
        paymentMethodScene.tax = self.tax
        paymentMethodScene.total = self.total
        paymentMethodScene.paymentMethodId = ""
       
        self.navigationController?.pushViewController(paymentMethodScene, animated: true)
    }
    
    
    func getLatLong(countryName:String)  {

        let geocoder = CLGeocoder()
        geocoder.geocodeAddressString(countryName, completionHandler: {(placemarks, error) -> Void in
                            if((error) != nil){
                                print("Error", error ?? "")
                            }
                            if let placemark = placemarks?.first {
                                let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                                
                                self.latitude = "\(coordinates.latitude)"
                                self.longtitude = "\(coordinates.longitude)"
                                
                                
                                let address = (placemark.name ?? "") + " " + (placemark.subLocality ?? "" )
                                    let fullAddress = address + " " + (placemark.locality ?? "")
                                self.address = fullAddress
                                
                                let position = CLLocationCoordinate2DMake(coordinates.latitude,coordinates.longitude)
                                let marker = GMSMarker(position: position)

                                marker.map = self.mapView
                                let camera = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: 18)
                                self.mapView?.animate(to: camera)

                                marker.icon = GMSMarker.markerImage(with: .red)
                            }
                        })
    }
}


extension deliveryInfoVC : UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == locationSelectTF {
            self.view.endEditing(true)
            
            let citiesVC = CitiesVC()
            citiesVC.delegate = self

            citiesVC.modalTransitionStyle = .crossDissolve
            citiesVC.modalPresentationStyle = .overFullScreen
            self.present(citiesVC, animated: true, completion: nil)
            
            return false
        }
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        let datePicker = UIDatePicker()
        datePicker.locale = Locale(identifier: MOLHLanguage.currentAppleLanguage().contains("en") ? "en" : "ar")
        if textField == dateTF{
            datePicker.datePickerMode = .date
            dateTF.inputView = datePicker
            
            datePicker.minimumDate = Date()
            Formatter.dateFormat = "yyyy/MM/dd"
            if dateTF.text!.isEmpty{
                
                components = Calendar.current.dateComponents([.year , .month , .day ], from: datePicker.date)
                dateTF.text = "\(components.year  ?? 1 )/\(components.month ?? 1)/\(components.day ?? 2020)"
            }else{
                guard let date = Formatter.date(from:dateTF.text!) else {
                    return
                }
                datePicker.setDate(date, animated: false)
            }
            
            datePicker.addTarget(self, action: #selector(self.dateValueChanged), for: .valueChanged)
        }else if textField == timeTF{
            datePicker.datePickerMode = .time
            timeTF.inputView = datePicker
            Formatter.dateFormat = "hh:mm a"
            if timeTF.text!.isEmpty{
                timeTF.text = Formatter.string(from: datePicker.date)
            }else{
                //according to date format your date string
                guard let date = Formatter.date(from: textField.text!) else {
                    fatalError()
                }
                
                datePicker.setDate(date, animated: false)
            }
            
            datePicker.addTarget(self, action: #selector(self.timeValueChanged), for: .valueChanged)
        }
    }
    
    
    @objc func dateValueChanged(sender:UIDatePicker) {
        let components = Calendar.current.dateComponents([.year , .month , .day ], from: sender.date)
        dateTF.text = "\(components.year  ?? 1 )/\(components.month ?? 1)/\(components.day ?? 2020)"
    }
    
    @objc func timeValueChanged(sender:UIDatePicker) {
        Formatter.dateFormat = "hh:mm a"
        timeTF.text = Formatter.string(from: sender.date)
    }
    
    func setUpviewData(){
        self.mapView.layer.cornerRadius = 12
        self.totalLable.text    = "Total".localized
        self.taxLable.text      = "Tax".localized
        self.deliveryLable.text = "Delivery".localized

        let total = UserDefaults.standard.string(forKey: Constants.CoffeeConstant.totlaPrice.rawValue)
        let tax = UserDefaults.standard.string(forKey: Constants.CoffeeConstant.taxPrice.rawValue)
        
        self.totalValueLable.text = " \(total ?? "")" + " SAR".localized
        self.taxValueLable.text   = " \(tax ?? "")"   + " SAR".localized
        
    }
    
}


extension deliveryInfoVC : LocationSelectedProtocol {
    func popupValueSelected(enAddress: String, arAddress: String, lat: String, lng: String) {
        locationSelectTF.text = arAddress
        self.address = arAddress
        self.latitude = lat
        self.longtitude = lng
    }
}


extension deliveryInfoVC : GetShippingCost {
    func cityName(Name: String, cityID: Int) {
        self.locationSelectTF.text = Name
        self.cityID = cityID
        getLatLong(countryName: Name)
    }
    
    func selectCitiy(Select: Bool) {
        if Select {
            shipinngDetailsStack.isHidden = false
            mapView.isHidden = false
            purchaseButton.isEnabled = true
        }else{
            shipinngDetailsStack.isHidden = true
            mapView.isHidden = true
            purchaseButton.isEnabled = false
        }
    }
    
    func theCost(value: Double) { //+ " SAR".localized
        self.deliveryValueLable.text = "\(value)" + " SAR".localized
        self.deliveryValueFromApi = value
        userDefaults.setValue(value, forKey: "delivery")
    }
    
}



extension deliveryInfoVC : CLLocationManagerDelegate, GMSMapViewDelegate {

    func reverseGeocodeLocation(_ location: CLLocation) {
        let locale = Locale(identifier: "ar")
        
        CLGeocoder().reverseGeocodeLocation(location, preferredLocale: locale) { placemarks, error in
            
            if let placemark = placemarks?.first {
                DispatchQueue.main.async {
                    //  update UI here
                    let address = (placemark.name ?? "") + " " + (placemark.subLocality ?? "" )
                        let fullAddress = address + " " + (placemark.locality ?? "")
                    self.address = fullAddress
                    UIView.animate(withDuration: 0.25) {
                        self.view.layoutIfNeeded()
                    }
                }
            }
        }
    }
    

    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        marker.position = coordinate
        let location = CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        reverseGeocodeLocation(location)
        
        
    }
    
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        let location = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
        reverseGeocodeLocation(location)
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        let userLocation = locations.last
        marker.position = CLLocationCoordinate2D(latitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude)
        
        self.latitude = "\(userLocation!.coordinate.latitude)"
        self.longtitude = "\(userLocation!.coordinate.longitude)"
        
        let camera = GMSCameraPosition.camera(withLatitude: userLocation!.coordinate.latitude, longitude: userLocation!.coordinate.longitude, zoom: 15);
        self.mapView.camera = camera
        self.mapView.isMyLocationEnabled = true
        
        print("Latitude :- \(userLocation!.coordinate.latitude)")
        print("Longitude :-\(userLocation!.coordinate.longitude)")
        marker.map = self.mapView
        marker.title = ""
        //marker.icon = #imageLiteral(resourceName: "noun_Location")
        marker.isDraggable = true
        locationManager.stopUpdatingLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            manager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            manager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            // manager.requestWhenInUseAuthorization()
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            //  manager.requestWhenInUseAuthorization()
            break
        default:
            break
        }
    }
}
